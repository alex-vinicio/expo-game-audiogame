using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;

public class dialogueControlLevel1 : MonoBehaviour
{
    //public variables
    public TextMeshProUGUI dialogText;
    public GameObject canvasDialog;
    public GameObject buttonActionsDialog;

    private Queue<string> sentences;
    private string sentence;
    
    //private variables
    private int indexVoices,index=0;
    private int tipeNPCAudio;
    private int[] valueTimeVoice;
    private bool activeDialogue = false;
    private bool activeKeyEnter = false;

    void Start() {
        sentences = new Queue<string>();
    }

    void Update(){        
        if(activeDialogue){
            if(index <valueTimeVoice.Length){
                activeDialogue=false;
                StartCoroutine(activation(index));
            }
        }
    
        if(dialogText.text == sentence){
            buttonActionsDialog.SetActive(true);
            activeKeyEnter = true;
        }

        if (activeKeyEnter)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                DisplayNextStentence();
            }
        }

    }    

    //time for wait to reproduce sounds
    IEnumerator activation(int index){
        yield return new WaitForSeconds(valueTimeVoice[index]);
        activeDialogue=true;
        DisplayNextStentence();
        activeKeyEnter = false;
        buttonActionsDialog.SetActive(false);
    }

    //Start
    public void ActiveDialogueLevel1(Dialogue dialogue, int point){
        index = 0;
        indexVoices =0;
        tipeNPCAudio = point;
        valueTimeVoice = changueSoundDurationGeneral(point);
        canvasDialog.SetActive(true);//very important the first go gameObject or prefab active and lest continue animation

        sentences.Clear();
        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        activeDialogue = true;
        activeKeyEnter = true;
        DisplayNextStentence(); 
        
    }

    public void DisplayNextStentence(){
        if(sentences.Count == 0){
            EndDialogue();
            index = 0;
            return;
        }
        sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    //change
    public int[] changueSoundDurationGeneral(int tipeSound){
        if(tipeSound == 1){
            if(FindObjectOfType<voicesNPC>())
                return  FindObjectOfType<voicesNPC>().addIndexVoice();
            else{
                return null;
            }
        }else{
            return null;
        }
    }
     //change 
    public void changueSoundActivationGeneral(int tipeSound){
        if(tipeSound == 1){
            if(FindObjectOfType<voicesNPC>())
                FindObjectOfType<voicesNPC>().ReproduceSound(indexVoices);
        }else{
            //here more code
        }
    }

    IEnumerator TypeSentence(string sentence){
        dialogText.text = "";
        buttonActionsDialog.SetActive(false);
        activeKeyEnter = false; 
        changueSoundActivationGeneral(tipeNPCAudio);
        indexVoices++;
        foreach (char letter in sentence.ToCharArray())
        {
             dialogText.text += letter; //important add + for add in the TMP letter for letter
             yield return null;
        }
        index++; //count for control of index of durations audio vectors
    }

    void EndDialogue(){
        canvasDialog.SetActive(false);
        changueSoundActivationGeneral(tipeNPCAudio);
        activeDialogue=false;
    }
}
