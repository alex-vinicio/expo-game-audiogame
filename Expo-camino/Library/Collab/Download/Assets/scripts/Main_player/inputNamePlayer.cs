using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro; 
using UnityEngine.Windows.Speech;
//using System.Linq;  //library for use a dictionary
//using System; //function por word Action

public class inputNamePlayer : MonoBehaviour
{
    public TextMeshProUGUI titleName;
    public GameObject registerName;
    public TextMeshProUGUI valueName;
    public int[] timeVoicesLogin;
    public bool correctName=false;
    private bool verifyRegister = false;
    private string responseLogin;
    //varaibles for voice dictation
    protected DictationRecognizer dictationRecognizer;
    void Start()
    {
        registerName.SetActive(true);
        StartCoroutine(timiWaitLogin(0));
    }
    IEnumerator timiWaitLogin(int index){
        if(index == 1){
            titleName.text = "Bienvenido al gran mundo de Expo-camino, aqui aprenderemos la historia de Quito usando matematica, asi que vamos.";
        }else{
            if(index == 2){
                titleName.text = "Vaya tu ya has estado jugando, quieres comenzar una nueva partida o quieres continuar donde te quedaste? resp. Si o no";
            }else{
                if(index == 3){
                    titleName.text = "Continua en el mundo de Expo-camino y ayudame a terminar la travecia. empezaras donde te quedaste, suerte";
                }else{
                    if(index == 4){
                        titleName.text = "¿Dime cual es tu nombre, ejem: Alex?";
                    }else{
                        if(index == 5){
                            titleName.text = "No tienes internet, cargando juego clasico";
                        }
                    }
                }
            }
        }
        yield return new WaitForSeconds(timeVoicesLogin[index]);
        if(index == 0){
            StartDictationEngine();
        }else{
            if(index == 1){
                sceneInputNamePlayer();
            }else{
                if(index == 2){
                    StartDictationEngine();
                }else{
                   if(index == 3){
                       sceneInputNamePlayer();
                       //chargue data of player
                   }else{
                       if(index == 4){
                            StartDictationEngine();
                        }else{
                            StartCoroutine(timiWaitLogin(1));
                        }   
                   }
                }
            }
        }
    }

    private void DictationRecognizer_OnDictationHypothesis(string text)
    {
        //Debug.Log("Dictation hypothesis: " + text);
    }
    private void DictationRecognizer_OnDictationComplete(DictationCompletionCause completionCause)
    {
        switch (completionCause)
        {
            case DictationCompletionCause.TimeoutExceeded:
            case DictationCompletionCause.PauseLimitExceeded:
            case DictationCompletionCause.Canceled:
            case DictationCompletionCause.Complete:
                // Restart required
                CloseDictationEngine();
                StartDictationEngine();
                break;
            case DictationCompletionCause.UnknownError:
            case DictationCompletionCause.AudioQualityFailure:
            case DictationCompletionCause.MicrophoneUnavailable:
            case DictationCompletionCause.NetworkFailure:
                // Error
                CloseDictationEngine();
                break;
        }
    }
    private void DictationRecognizer_OnDictationResult(string text, ConfidenceLevel confidence)
    {
        valueName.text = text;
        CloseDictationEngine();
        Debug.Log(valueName.text);
        if(valueName.text == "sí"){
            correctName = true;
            StartCoroutine(timiWaitLogin(4));
        }else{
            if(valueName.text == "no"){
                correctName = true;
                StartCoroutine(timiWaitLogin(3));
            }else{
                correctName = true;
                StartCoroutine(newUser());
            }
        }
    }
    IEnumerator newUser(){
        WWWForm formU= new WWWForm();
        formU.AddField("name", valueName.text);
        formU.AddField("nivelVision","bajo");
        WWW www = new WWW("http://localhost/sqlconnect/registerU.php", formU);
        yield return www;
        print(www.text);
        if(www.text[0] == '0'){
            Debug.Log("Usuario registrado correctamente!");
            playerData.namePlayer = valueName.text;
            playerData.numerPosibility = 5;
            playerData.score = 0;
            //playerData.checkPoint = [0,0,0]
            playerData.respuestasFallidas = 0;
            playerData.conexionInternet = true;
            StartCoroutine(timiWaitLogin(1));
        }else{
            if(www.text == "existe"){
                StartCoroutine(timiWaitLogin(2));
            }else{
                StartCoroutine(timiWaitLogin(5));
            }
        }
    }
    private void DictationRecognizer_OnDictationError(string error, int hresult)
    {
        Debug.Log("Dictation error: " + error);
    }
    private void OnApplicationQuit()
    {
        CloseDictationEngine();
    }
    private void StartDictationEngine()
    {
        dictationRecognizer = new DictationRecognizer();
        dictationRecognizer.DictationHypothesis += DictationRecognizer_OnDictationHypothesis;
        dictationRecognizer.DictationResult += DictationRecognizer_OnDictationResult;
        dictationRecognizer.DictationComplete += DictationRecognizer_OnDictationComplete;
        dictationRecognizer.DictationError += DictationRecognizer_OnDictationError;
        dictationRecognizer.Start();
    }
    private void CloseDictationEngine()
    {
        if (dictationRecognizer != null)
        {
            dictationRecognizer.DictationHypothesis -= DictationRecognizer_OnDictationHypothesis;
            dictationRecognizer.DictationComplete -= DictationRecognizer_OnDictationComplete;
            dictationRecognizer.DictationResult -= DictationRecognizer_OnDictationResult;
            dictationRecognizer.DictationError -= DictationRecognizer_OnDictationError;
            if (dictationRecognizer.Status == SpeechSystemStatus.Running)
            {
                dictationRecognizer.Stop();
            }
            dictationRecognizer.Dispose();
        }
    }
    public void sceneInputNamePlayer(){
        if(correctName){
            StartCoroutine(waitForInputName());
        }
    }
    IEnumerator waitForInputName(){
        yield return new WaitForSeconds(2);
        registerName.SetActive(false);
        FindObjectOfType<levelLoad>().LoadLevel(1); //sent a scene for charge
        yield return null;
    }
}
