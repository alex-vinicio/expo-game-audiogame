using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MainPlayerMovement : MonoBehaviour
{
    public float moveSpeed = 5f;
    public Rigidbody2D rb;//use for controle player but grabity = 0
    public Animator animator;
    public bool noMovement;
    public bool introState = false;
    private int activeHorizontal = 0;
    private int activeVertical = 0;
    private float walkValue=0;
    Vector2 movement;
    
    void Update()
    {
        //pres input, this is determinate for fotogram
        if(noMovement){
            if(introState){
                if(activeHorizontal == 1 ){
                    movement.x = Input.GetAxisRaw("Horizontal");
                    animator.SetFloat("Horizontal", movement.x);//add value 1 or -1 at eje X
                }else{
                    if(activeHorizontal == 0){
                        movement.y = Input.GetAxisRaw("Vertical");
                        animator.SetFloat("Vertical", movement.y);
                    }
                }
                animator.SetFloat("Speed", movement.sqrMagnitude);
            }else{
                movement.x = Input.GetAxisRaw("Horizontal");
                movement.y = Input.GetAxisRaw("Vertical");
                animator.SetFloat("Horizontal", movement.x);//add value 1 or -1 at eje X
                animator.SetFloat("Vertical", movement.y);//same process but add value at eje Y and generate animation of specific configuration inAnimator of player
                animator.SetFloat("Speed", movement.sqrMagnitude);
            }
        }else
        {
            movement.x = 0;
            movement.y = 0;
            animator.SetFloat("Horizontal", movement.x);//add value 1 or -1 at eje X
            animator.SetFloat("Vertical", movement.y);//same process but add value at eje Y and generate animation of specific configuration inAnimator of player
            animator.SetFloat("Speed", movement.sqrMagnitude);
        }
    }

    void FixedUpdate(){
        rb.MovePosition(rb.position + movement * moveSpeed* Time.deltaTime);//add value at position whit value of movement and speed 
        
        // print(Mathf.Abs(transform.position.x)+" *** "+Mathf.Abs(transform.position.y));
        // walkValue = Convert.ToInt64(Math.Floor((Math.Abs(transform.position.x) + Math.Abs(transform.position.y))/2));//aplicate a absolute value for change -  for +
        // print(walkValue);
    }

    public void ActivateMoveControll(bool activateValue, bool introValue, int valueHorizontal, int valueVertical){

        noMovement = activateValue;
        introState = introValue;
        activeHorizontal = valueHorizontal;
        activeVertical = valueVertical;
    }
}
