using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro; 
using UnityEngine.Windows.Speech;
//using System.Linq;  //library for use a dictionary
//using System; //function por word Action

public class inputNamePlayer : MonoBehaviour
{
    public TextMeshProUGUI titleName;
    public GameObject registerName;
    public TextMeshProUGUI valueName;
    public int[] timeVoicesLogin;
    public bool correctName=false;
    private bool verifyRegister = false;
    private string responseLogin;
    private string nameRequest;
    //audio
    public AudioClip[] voices;
    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;
    //varaibles for voice dictation
    protected DictationRecognizer dictationRecognizer;
    void Start()
    {
        registerName.SetActive(true);
        StartCoroutine(timiWaitLogin(0));//time transition for wait or pause the text dialogue and replay voice
    }
    public void soundPlay(int index){//function for play the audio
        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }
    IEnumerator timiWaitLogin(int index){
        if(index == 1){
            titleName.text = "Bienvenido al gran mundo de Expo-camino, aqui aprenderemos la historia de Quito usando matematica, asi que vamos.";
            soundPlay(index);
        }else{
            if(index == 2){
                titleName.text = "Vaya tu ya has estado jugando, quieres comenzar una nueva partida o quieres continuar donde te quedaste? resp. Si o no";
                soundPlay(index);
            }else{
                if(index == 3){
                    titleName.text = "Continua en el mundo de Expo-camino y ayudame a terminar la travecia. empezaras donde te quedaste, suerte";
                    soundPlay(index);
                }else{
                    if(index == 4){
                        titleName.text = "¿Dime cual es tu nombre, ejem: Alex?";
                        soundPlay(index);
                    }else{
                        if(index == 5){
                            titleName.text = "No tienes internet, cargando juego clasico";
                            soundPlay(index);
                        }
                    }
                }
            }
        }
        yield return new WaitForSeconds(timeVoicesLogin[index]);
        voiceSource.Stop();
        if(index == 0){
            StartDictationEngine();
        }else{
            if(index == 1){
                sceneInputNamePlayer();
            }else{
                if(index == 2){
                    StartDictationEngine();
                }else{
                   if(index == 3){
                       //startCoroutine(chargeDataPlayer());
                       sceneInputNamePlayer();
                       //chargue data of player
                   }else{
                       if(index == 4){
                            StartDictationEngine();
                        }else{
                            StartCoroutine(timiWaitLogin(1));
                        }   
                   }
                }
            }
        }
    }
    
    IEnumerator chargeDataPlayer(){
        WWWForm formU= new WWWForm();//create a form for sent Post of server
        formU.AddField("name", valueName.text);
        WWW www = new WWW("http://localhost/sqlconnect/gestUserData.php", formU);//URL of service registered
        yield return www;
        print(www.text);
        if(www.text != null){
            print("guardando datos");
        }else
        {
            StartCoroutine(timiWaitLogin(4));
        }
    }
    private void DictationRecognizer_OnDictationHypothesis(string text)
    {
        //Debug.Log("Dictation hypothesis: " + text);
    }
    private void DictationRecognizer_OnDictationComplete(DictationCompletionCause completionCause)
    {
        switch (completionCause)
        {
            case DictationCompletionCause.TimeoutExceeded:
            case DictationCompletionCause.PauseLimitExceeded:
            case DictationCompletionCause.Canceled:
            case DictationCompletionCause.Complete:
                // Restart required
                CloseDictationEngine();
                StartDictationEngine();
                break;
            case DictationCompletionCause.UnknownError:
            case DictationCompletionCause.AudioQualityFailure:
            case DictationCompletionCause.MicrophoneUnavailable:
            case DictationCompletionCause.NetworkFailure:
                // Error
                CloseDictationEngine();
                break;
        }
    }
    private void DictationRecognizer_OnDictationResult(string text, ConfidenceLevel confidence)
    {
        valueName.text = text;
        CloseDictationEngine();
        if(valueName.text == "sí"){
            correctName = true;
            StartCoroutine(timiWaitLogin(4));
        }else{
            if(valueName.text == "no"){
                correctName = true;
                StartCoroutine(timiWaitLogin(3));
            }else{
                correctName = true;
                StartCoroutine(newUser());
            }
        }
    }
    IEnumerator newUser(){
        WWWForm formU= new WWWForm();//create a form for sent Post of server
        formU.AddField("name", valueName.text);
        formU.AddField("nivelVision","bajo");
        WWW www = new WWW("http://localhost/sqlconnect/registerU.php", formU);//URL of service registered
        yield return www;
        print(www.text);
        if(www.text != ""){
            if(www.text[0] == '0'){//if the name of player is new
                Debug.Log("Usuario registrado correctamente!");
                playerData.namePlayer = valueName.text;
                playerData.numerPosibility = 5;
                playerData.score = 0;
                //playerData.checkPoint = [0,0,0]
                playerData.respuestasFallidas = 0;
                playerData.conexionInternet = true;
                StartCoroutine(timiWaitLogin(1));
            }else{//if name of player is registered in DB
                if(www.text == "existe"){
                    nameRequest = valueName.text;
                    StartCoroutine(timiWaitLogin(2));
                }
            }  
        }else{
            StartCoroutine(timiWaitLogin(5));//if the player don't have internet or the server is off
        }
    }
    private void DictationRecognizer_OnDictationError(string error, int hresult)
    {
        Debug.Log("Dictation error: " + error);
    }
    private void OnApplicationQuit()
    {
        CloseDictationEngine();
    }
    private void StartDictationEngine()
    {
        dictationRecognizer = new DictationRecognizer();
        dictationRecognizer.DictationHypothesis += DictationRecognizer_OnDictationHypothesis;
        dictationRecognizer.DictationResult += DictationRecognizer_OnDictationResult;
        dictationRecognizer.DictationComplete += DictationRecognizer_OnDictationComplete;
        dictationRecognizer.DictationError += DictationRecognizer_OnDictationError;
        dictationRecognizer.Start();
    }
    private void CloseDictationEngine()
    {
        if (dictationRecognizer != null)
        {
            dictationRecognizer.DictationHypothesis -= DictationRecognizer_OnDictationHypothesis;
            dictationRecognizer.DictationComplete -= DictationRecognizer_OnDictationComplete;
            dictationRecognizer.DictationResult -= DictationRecognizer_OnDictationResult;
            dictationRecognizer.DictationError -= DictationRecognizer_OnDictationError;
            if (dictationRecognizer.Status == SpeechSystemStatus.Running)
            {
                dictationRecognizer.Stop();
            }
            dictationRecognizer.Dispose();
        }
    }
    public void sceneInputNamePlayer(){
        if(correctName){
            StartCoroutine(waitForInputName());
        }
    }
    IEnumerator waitForInputName(){
        yield return new WaitForSeconds(2);
        registerName.SetActive(false);
        FindObjectOfType<levelLoad>().LoadLevel(1); //sent a scene for charge
        yield return null;
    }
}
