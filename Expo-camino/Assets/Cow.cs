using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cow : MonoBehaviour
{
    public Dialogue dialogue;

    public void TriggerCowNPC(MainPlayer playerUnit){
        FindObjectOfType<DialogueManagerIntro>().StartDialogueCow(dialogue, playerUnit);
    }

    void OnTriggerEnter2D(Collider2D hitInfo){
        MainPlayer playerUnit =  hitInfo.GetComponent<MainPlayer>();

        if(hitInfo.gameObject.tag == "Player"){
            TriggerCowNPC(playerUnit);
        }
    }
}
