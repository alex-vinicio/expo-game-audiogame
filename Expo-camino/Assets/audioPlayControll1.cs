using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioPlayControll1 : MonoBehaviour
{
    public AudioSource sonidoCampana;
    public GameObject point1;

    void Start()
    {
        //sonidoCampana = GetComponent<AudioSource>();
    }

    void OnTriggerEnter2D(Collider2D hitInfo){
        MainPlayer playerUnit =  hitInfo.GetComponent<MainPlayer>();

        if(hitInfo.gameObject.tag == "Player"){
            sonidoCampana.Stop();
            point1.SetActive(false);
        }
    }
}
