using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monjaNpcAudio : MonoBehaviour
{
    private DialogueManagerIntro dialogueManager;

    public AudioClip[] voices;
    public int[] secondsAudios;

    public GameObject mainPlayer;
    public GameObject nextPointChurch;
    //acces to points o references
    public int timePauseDialogue;
    public GameObject canvasQuestions;

    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;
    //control of the point
    public int pointBook;

    void Start()
    {
        dialogueManager = GetComponent<DialogueManagerIntro>();
    }

    //It use for add vector seconts in other class
    public int[] addIndexVoice()
    {
        return secondsAudios;
    }
    public void ReproduceSound(int index)
    {
        if (index >= secondsAudios.Length)
        {
            voiceSource.Stop();
            StartCoroutine(waitForPauseVoice());
            return;
        }
        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }

    IEnumerator waitForPauseVoice()
    {

        yield return new WaitForSeconds(timePauseDialogue);
        canvasQuestions.SetActive(true);
        FindObjectOfType<moveSelection>().startPosition();//restar the position of selecction of the answer's value
        FindObjectOfType<questionsPointLvl2>().activeQuestionsP1Lvl2();

        /*if (pointBook == 5)
        {
            FindObjectOfType<updateValueLvl2>().scoreValuePlayer(20);
            player.ActivateMoveControll(false, false, 0, 0);
            FindObjectOfType<audioEffectDoors>().activeFirstStateCloseDoor();//active close door animation transition
            FindObjectOfType<animationPanelChanguePositionPlayer>().asignPositionValues(86.9f, 128.5f);//asing value of move position in church1
            FindObjectOfType<animationPanelChanguePositionPlayer>().activePanelEffectAnimation();
        }*/
    }
}
