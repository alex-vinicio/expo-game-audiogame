using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class finalGameDialogue : MonoBehaviour
{
    public GameObject beforePointSound;

    [Space]
    public AudioSource audioSource;
    public AudioClip[] listSounds;
    public int[] durationSounds;
    public GameObject canvasTable;

    private int countIndexAudios = 0;
    private bool activeAudio = false;

    // Start is called before the first frame update
    public void startFinalAudio(){
        beforePointSound.SetActive(false);
        StartCoroutine(waitTimeActiveSound());
    }

    void Update(){
        if(activeAudio){  
            activeAudio = false;  
            if(countIndexAudios < listSounds.Length){
                StartCoroutine(dialogueSoundActive(countIndexAudios));
            }
        }else{
            if(countIndexAudios >= listSounds.Length){
                StopAllCoroutines();
                audioSource.Stop();
                canvasTable.SetActive(false);
                FindObjectOfType<levelLoad>().LoadLevel(0);
            }
        }
    }

    IEnumerator waitTimeActiveSound(){
        yield return new WaitForSeconds(1);
        continueDialogue();
    }
    void continueDialogue(){
        StartCoroutine(dialogueSoundActive(0));
    }
    IEnumerator dialogueSoundActive(int index){
        audioSource.Stop();
        audioSource.clip = listSounds[index];
        audioSource.Play();
        yield return new WaitForSeconds(durationSounds[index]);
        countIndexAudios++;
        activeAudio = true;
    }
}
