using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerLegend : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        MainPlayer playerUnit = hitInfo.GetComponent<MainPlayer>();
        if (hitInfo.gameObject.tag == "Player")
        {
            MainPlayerMovement player = playerUnit.GetComponent<MainPlayerMovement>();
            player.ActivateMoveControll(false, false, 0, 0);
            FindObjectOfType<monjaNpcText>().activateDialogueMonja(2);
        }
    }
}
