using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerCow : MonoBehaviour
{
    private DialogueManagerIntro dialogueManager;

    public AudioClip[] voices;
    public AudioClip[] punctuations;
    public int[] secondsAudios;
    //private bool restriction = false;

    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;

    void Start()
    {
        dialogueManager = GetComponent<DialogueManagerIntro>();
    }

    public int[] valueTimeVoices(){//It use for add vector seconts in other class
        return secondsAudios;
    }

    public void ReproduceSound(int index){
        if(index >= secondsAudios.Length){
            voiceSource.Stop();
            return;
        }

        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }
}
