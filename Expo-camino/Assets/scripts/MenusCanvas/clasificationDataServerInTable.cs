using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;
using System.Globalization;

public class clasificationDataServerInTable : MonoBehaviour
{
    public GameObject beforeObject;
    public GameObject afterObject;

    [Space]
    public AudioSource audioSource;
    public AudioClip[] listSounds;
    public int[] durationTimeAudio;
    //tmpro of the list of the best five players
    public TextMeshProUGUI name1;
    public TextMeshProUGUI name2;
    public TextMeshProUGUI name3;
    public TextMeshProUGUI name4;
    public TextMeshProUGUI name5;
    public TextMeshProUGUI score1;
    public TextMeshProUGUI score2;
    public TextMeshProUGUI score3;
    public TextMeshProUGUI score4;
    public TextMeshProUGUI score5;
    public TextMeshProUGUI position1;
    public TextMeshProUGUI position2;
    public TextMeshProUGUI position3;
    public TextMeshProUGUI position4;
    public TextMeshProUGUI position5;

    private string[] vectorNames = new string[6];
    private string[] vectorScores = new string[6];
    private string[] vectorPositions = new string[6];

    public void activeInputDataInTablePosition(){
        beforeObject.SetActive(false);
        StartCoroutine(verifyConectServer());
    }
    IEnumerator verifyConectServer(){
        UnityWebRequest www = UnityWebRequest.Get("http://expocaminobe.softjumpai.online/validateInternetConection/serverOn.php");//URL of service registered
        yield return www.SendWebRequest();//add new tecnologi replace a WWW obsolet
        switch (www.result)
        {
            case UnityWebRequest.Result.ConnectionError:
                StartCoroutine(waitAudio(7));
                break;
            case UnityWebRequest.Result.DataProcessingError:
                StartCoroutine(waitAudio(7));
                break;
            case UnityWebRequest.Result.ProtocolError:
                StartCoroutine(waitAudio(7));
                break;
            case UnityWebRequest.Result.Success:
                StartCoroutine(chargeDataPlayer());
                break;
        }
    }

    IEnumerator chargeDataPlayer(){
        WWWForm formU= new WWWForm();//create a form for sent Post of server
        formU.AddField("name", playerData.namePlayer);
        formU.AddField("idLevel", playerData.nivelActual);
        UnityWebRequest www = UnityWebRequest.Post("http://expocaminobe.softjumpai.online/levelquest/getTableDataPositions.php", formU);//URL of service registered
        yield return www.SendWebRequest();//add new tecnologi replace a WWW obsolet
        if(www.downloadHandler.text[0] == '0'){//to assign variable player to charge in player
            int indexArrayRequest = 0; 
            int index1=1, index2=2,index3=3;
            int valuePositionPlayer = 7;
           do{
               if(indexArrayRequest == 5){//register position of the player
                    valuePositionPlayer = int.Parse(www.downloadHandler.text.Split('\t')[index3]);
                }

                vectorNames[indexArrayRequest] = www.downloadHandler.text.Split('\t')[index1];
                vectorScores[indexArrayRequest] = www.downloadHandler.text.Split('\t')[index2];
                vectorPositions[indexArrayRequest] = www.downloadHandler.text.Split('\t')[index3];
                index1 = index3 + 1;
                index2 = index3 + 2;
                index3 = index3 + 3;
                
               indexArrayRequest ++;
           }while(indexArrayRequest < 6);

            assignValueInTMpros();
            StartCoroutine(waitAudio((valuePositionPlayer-1)));
        }else{
            StartCoroutine(waitAudio(7));
        }
    }
    void assignValueInTMpros(){
        //names
        name1.text = vectorNames[0];
        name2.text = vectorNames[1];
        name3.text = vectorNames[2];
        name4.text = vectorNames[3];
        name5.text = vectorNames[4];
        //score
        score1.text = vectorScores[0];
        score2.text = vectorScores[1];
        score3.text = vectorScores[2];
        score4.text = vectorScores[3];
        score5.text = vectorScores[4];
        //position
        position1.text = vectorPositions[0];
        position2.text = vectorPositions[1];
        position3.text = vectorPositions[2];
        position4.text = vectorPositions[3];
        position5.text = vectorPositions[4];
    }
    IEnumerator waitAudio(int index){
        audioSource.Stop();
        audioSource.clip = listSounds[index];
        audioSource.Play();
        yield return new WaitForSeconds(durationTimeAudio[index]);
        afterObject.SetActive(true);
        finalGameDialogue ob = afterObject.GetComponent<finalGameDialogue>();
        ob.startFinalAudio();
    }

}
