using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startVoicePuntuation : MonoBehaviour
{
    [Space]
    public AudioSource audio;
    public AudioClip[] voices;
    public int[] timeVoices;

    public GameObject afterObject;

    private int countAudio;
    private bool controlAudio = false;

    void Start(){
        countAudio = 0;
        startAudio(countAudio);
        StartCoroutine(timeStart(0));
    }
    void Update(){
        if (controlAudio)
        {
            countAudio++;
            controlAudio = false;
            startAudio(countAudio);
        }
    }
    IEnumerator timeStart(int indexAudio){
        yield return new WaitForSeconds(timeVoices[indexAudio]);
        controlAudio = true;
    }
    public void startAudio(int indexAudio){
        if(indexAudio >= voices.Length){
            afterObject.SetActive(true);
            clasificationDataServerInTable cd = afterObject.GetComponent<clasificationDataServerInTable>();
            cd.activeInputDataInTablePosition();
            return;
        }
        audio.Stop();
        audio.clip = voices[indexAudio];
        audio.Play();
        StartCoroutine(timeStart(indexAudio));
    }
}
