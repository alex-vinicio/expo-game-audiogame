using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class animationPanelChanguePositionPlayer : MonoBehaviour
{
    public Animator animator;
    //player varables
    public Transform rbc;

    public float moveSpeed=5f;
    public float[] valuePositions; 
    Vector2 movement;

    public void activePanelEffectAnimation(){
        
        animator.SetBool("onPanelForPlayer", true);
        
    }

    public void desactivePanelEffectAnimation(){
        animator.SetBool("onPanelForPlayer", false);
    }

    public void asignPositionValues(float x, float y){
        valuePositions[0] = x;
        valuePositions[1] = y;
    }

    public void movePlayerIntoChrurch(){
        movement.x = valuePositions[0];
        movement.y = valuePositions[1];

        rbc.position = movement;
    }
}
