using System.Collections;
//using System.Collections.Generic;  //this library don't use
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro; 

public class levelLoad : MonoBehaviour
{
    public GameObject loadingScreen;
    public Slider slider;
    public TextMeshProUGUI progressText;
    

    public void LoadLevel (int sceneIndex){//se puede llamar cualquier otro nombre
        StartCoroutine(LoadAsynchronously(sceneIndex));
    }

    IEnumerator LoadAsynchronously(int sceneIndex){
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        loadingScreen.SetActive(true);
        
        while (!operation.isDone){
            float progress = Mathf.Clamp01(operation.progress / .9f);//divide el valor de progress a 0.9, donde se optiene el valor para el slider, si llega a 1 el slider muestra la carga completa y sale del bucle
            slider.value = progress;
            progressText.text = progress * 100f + "%";
            
            yield return null;
        }
    }
}
