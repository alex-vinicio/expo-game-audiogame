using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class positionChange : MonoBehaviour
{
    //animation pointer
    public Image pointerMove;
    Vector2 movement;
    public void animationPointer1(){

        movement = pointerMove.rectTransform.localPosition;
        pointerMove.rectTransform.localPosition = new Vector2 ( (float) movement.x+10.66f,(float) movement.y); //(-302.5)  
    }

    public void animationPointer2(){
        movement = pointerMove.rectTransform.localPosition;
        pointerMove.rectTransform.localPosition = new Vector2 ( (float) (movement.x+10.66f),movement.y);//-12,66(-289.84)
    }

    public void animationPointer3(){
        movement = pointerMove.rectTransform.localPosition;
        pointerMove.rectTransform.localPosition = new Vector2 ((float)(movement.x-10.66f),movement.y); //(-274.5) -28
    }

    public void animationPointer4(){
        movement = pointerMove.rectTransform.localPosition;
        pointerMove.rectTransform.localPosition = new Vector2 ((float)(movement.x-10.66f),movement.y); //(-274.5) -28
    }

    public void animationStart(){//section for restar position inthe first animation
        movement = pointerMove.rectTransform.localPosition;
        pointerMove.rectTransform.localPosition = new Vector2 (-321,movement.y);
    }
}
