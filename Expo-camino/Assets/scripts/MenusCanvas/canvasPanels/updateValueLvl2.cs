using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class updateValueLvl2 : MonoBehaviour
{
    //object text value
    public TextMeshProUGUI namePlayer;
    public TextMeshProUGUI lifePoint;
    public TextMeshProUGUI scoreValue;
    public TextMeshProUGUI timeInLevel;
    public TextMeshProUGUI coinPoint;
    //private variables
    private int saveTimeForLevel;
    // awake is necessary for don't destroy object in the load scene
    void Awake()
    {
        saveTimeForLevel = (int) playerData.tiempoNivel;
        namePlayer.text = playerData.namePlayer;
        lifePointPlayer(playerData.numRepeticion);
        //scoreValuePlayer(playerData.score);
        coinPointPlayer(playerData.coins);
    }

    // Update is called once per frame
    void Update()
    {
        timeInLevelPlayer(Time.timeSinceLevelLoad);
    }

    public void lifePointPlayer(int life){
        lifePoint.text =  life.ToString();
    }
    public void scoreViewtPlayer(int score){
        scoreValue.text = score.ToString();
    }
    public void scoreValuePlayer(int score){
        scoreValue.text = FindObjectOfType<MainPlayer>().scoreAdd(score).ToString();
    }
    public void timeInLevelPlayer(float time){
        int timeConvert = (int)time;
        timeInLevel.text = timeConvert.ToString();
        saveTimeForLevel = timeConvert;
        playerData.tiempoNivel = saveTimeForLevel;
    }
    public int getTimeLevel(){
        return saveTimeForLevel;
    }
    public void coinPointPlayer(int coins){
        coinPoint.text = ": "+FindObjectOfType<MainPlayer>().coinAdd(coins).ToString();
    }

    public void saveLessesScoreVariableGeneral(int score){
        playerData.score =  playerData.score + score;
        scoreValue.text = FindObjectOfType<MainPlayer>().scoreAdd(score).ToString();
    }

    public void saveWinSscoreVariableGeneral(int score){
        playerData.score = FindObjectOfType<MainPlayer>().scoreAdd(score);//replace local variable of score in global variable
    }

}
