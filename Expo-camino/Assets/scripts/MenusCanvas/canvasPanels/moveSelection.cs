using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class moveSelection : MonoBehaviour
{
    public Image pointerMove;
    Vector2 movement;

    public void startPosition(){
        movement = pointerMove.rectTransform.localPosition;
        pointerMove.rectTransform.localPosition = new Vector2 ( (float)(-270.3891f),(float) movement.y);
    }

    public void rightPosition(){

        movement = pointerMove.rectTransform.localPosition;
        pointerMove.rectTransform.localPosition = new Vector2 ( (float)(movement.x +248.05f),(float) movement.y); //(-302.5)   //-274.1 -- 222
    }

    public void leftPosition(){

        movement = pointerMove.rectTransform.localPosition;
        pointerMove.rectTransform.localPosition = new Vector2 ( (float)(movement.x -248.05f),(float) movement.y); //(-302.5)  
    }

}
