using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dialogueMenu : MonoBehaviour
{
    public int[] timeVoicesLogin;
    public AudioClip[] voices;
    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;

    //object buttons
    public Image buttonAnima;
    public GameObject image1;
    public GameObject image2;
    public GameObject image3;
    public GameObject image4;
    //menu transition
    public GameObject mainMenu;
    public GameObject optionMenu;

    private int countPressButton = 0;
    // Start is called before the first frame update
    void Awake()
    {
        voiceTransition(0);
        ColorButton(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("s") || Input.GetKeyDown("down"))
        {
            if(countPressButton < 3){
                countPressButton++;
                ColorButton(countPressButton);
                voiceTransition(countPressButton);
            }
        }
        if(Input.GetKeyDown("w") || Input.GetKeyDown("up")){
            if(countPressButton > 0){
                countPressButton--;
                ColorButton(countPressButton);
                voiceTransition(countPressButton);
            }
        }
        
        if(Input.GetKeyDown(KeyCode.Return)){
            if(countPressButton !=0 ){
                voiceSource.Stop();
                if(countPressButton == 1){
                    FindObjectOfType<mainMenu>().abiliteControlVoices();
                }else{
                    if(countPressButton == 2){
                        mainMenu.SetActive(false);
                        optionMenu.SetActive(true);
                        FindObjectOfType<dialogueMenuOption>().changueCountOption(0);
                    }else{
                        FindObjectOfType<mainMenu>().quitGame();
                    }
                }  
            }
        }
    }
    public void ColorButton(int control){
        if(control == 0){
            image1.SetActive(true);
            image2.SetActive(false);
            image3.SetActive(false);
            image4.SetActive(false);
        }else{
            if(control == 1){
                image1.SetActive(false);
                image2.SetActive(true);
                image3.SetActive(false);
                image4.SetActive(false);
            }else{
                if(control == 2){
                    image1.SetActive(false);
                    image2.SetActive(false);
                    image3.SetActive(true);
                    image4.SetActive(false);
                }else{
                    if(control == 3){
                        image1.SetActive(false);
                        image2.SetActive(false);
                        image3.SetActive(false);
                        image4.SetActive(true);
                    }
                }
            }
        }
        
    }

    void voiceTransition(int index){
        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }

    public void changueCount(int count){
        countPressButton = count;
        voiceTransition(countPressButton);
        ColorButton(count);
    }
}
