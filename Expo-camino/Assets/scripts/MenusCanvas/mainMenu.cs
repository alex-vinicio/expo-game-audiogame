using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro; 

public class mainMenu : MonoBehaviour
{
    public GameObject controlObjectVoice;
    public GameObject canvasMenu;

    public void playGame(){
        SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex + 1);//permite cargar misma escena o especfica.
    }

    public void quitGame(){
        Debug.Log("saliendo");
        Application.Quit();
    }

    public void abiliteControlVoices(){
        canvasMenu.SetActive(false);
        controlObjectVoice.SetActive(true);
    }
}
