using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dialogueMenuOption : MonoBehaviour
{
    public int[] timeVoicesLogin;
    public AudioClip[] voices;
    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;

    //object buttons
    public GameObject image1;
    public GameObject image2;
    public GameObject image3;

    //menu transition
    public GameObject mainMenu;
    public GameObject optionMenu;

    private int countPressButton = 0;
    // Start is called before the first frame update
    void Start()
    {
        countPressButton = 0;
        voiceTransition(0);
        ColorButton(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("s") || Input.GetKeyDown("down"))
        {
            if(countPressButton < 2){
                countPressButton++;
                ColorButton(countPressButton);
                voiceTransition(countPressButton);
            }
        }
        if(Input.GetKeyDown("w") || Input.GetKeyDown("up")){
            if(countPressButton > 0){
                countPressButton--;
                ColorButton(countPressButton);
                voiceTransition(countPressButton);
            }
        }
        
        if(Input.GetKeyDown(KeyCode.Return)){
            if(countPressButton !=0 ){
                voiceSource.Stop();
                if(countPressButton == 1){
                    //logica para subir o bajar el volumen
                    valueVolumeGeneral.volumeValue = 1;
                }else{
                    if(countPressButton == 2){
                        ColorButton(0);
                        mainMenu.SetActive(true);
                        optionMenu.SetActive(false);
                        FindObjectOfType<dialogueMenu>().changueCount(0);
                        voiceSource.Stop();
                    }
                }  
            }
        }
    }
    public void ColorButton(int control){
        if(control == 0){
            image1.SetActive(true);
            image2.SetActive(false);
            image3.SetActive(false);
        }else{
            if(control == 1){
                image1.SetActive(false);
                image2.SetActive(true);
                image3.SetActive(false);
            }else{
                if(control == 2){
                    image1.SetActive(false);
                    image2.SetActive(false);
                    image3.SetActive(true);
                }
            }
        }
        
    }

    void voiceTransition(int index){
        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }

    public void changueCountOption(int count){
        countPressButton = count;
        voiceTransition(countPressButton);
        ColorButton(count);
    }
}
