using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class voicesNPC : MonoBehaviour
{
    private DialogueManagerIntro dialogueManager;

    public AudioClip[] voices;
    public int[] secondsAudios;

    public GameObject mainPlayer;
    public GameObject nextPoint;
    //acces to points o references
    public int timePauseDialogue;

    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;

    void Start()
    {
        dialogueManager = GetComponent<DialogueManagerIntro>();
    }

    //It use for add vector seconts in other class
    public int[] addIndexVoice()
    {
        return secondsAudios;
    }

    public void ReproduceSound(int index)
    {
        if (index >= secondsAudios.Length)
        {
            voiceSource.Stop();
            StartCoroutine(waitForPauseVoice());
            return;
        }
        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }

    IEnumerator waitForPauseVoice()
    {

        yield return new WaitForSeconds(timePauseDialogue);

        MainPlayerMovement player = mainPlayer.GetComponent<MainPlayerMovement>();
        player.ActivateMoveControll(true, false, 0, 0);
        nextPoint.SetActive(true);
    }
}
