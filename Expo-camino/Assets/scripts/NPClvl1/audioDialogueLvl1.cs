using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioDialogueLvl1 : MonoBehaviour
{
    private DialogueManagerIntro dialogueManager;

    public AudioClip[] voices;
    public int[] secondsAudios;

    public GameObject mainPlayer;
    //acces to points o references
    public int timePauseDialogue;
    public GameObject canvasQuestions;

    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;

    void Start()
    {
        dialogueManager = GetComponent<DialogueManagerIntro>();
    }

    //It use for add vector seconts in other class
    public int[] addIndexVoice()
    {
        return secondsAudios;
    }

    public void ReproduceSound(int index)
    {
        if (index >= secondsAudios.Length)
        {
            StopAllCoroutines();
            voiceSource.Stop();
            StartCoroutine(waitForPauseVoice());
            return;
        }
        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }

    IEnumerator waitForPauseVoice()
    {
        yield return new WaitForSeconds(timePauseDialogue);

        canvasQuestions.SetActive(true);
        FindObjectOfType<moveSelection>().startPosition();//restar the position of selecction of the answer's value
        FindObjectOfType<questionsPointLvl2>().activeQuestionsP1Lvl2();
    }
}
