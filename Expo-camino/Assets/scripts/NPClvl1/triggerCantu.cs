using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerCantu : MonoBehaviour
{
    public Dialogue dialogue;
    public GameObject playerGame;
    
    public int pointC;

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        MainPlayer playerUnit = hitInfo.GetComponent<MainPlayer>();
        if (hitInfo.gameObject.tag == "Player")
        {
            MainPlayerMovement player = playerUnit.GetComponent<MainPlayerMovement>();
            player.ActivateMoveControll(false, false, 0, 0);
            if (pointC == 1)
            {
                FindObjectOfType<ControlDialog>().StartDialogueNpcFirst(dialogue, 4);
            }
            else
            {
                if (pointC == 2)
                {
                    FindObjectOfType<dialogeLvl1>().activateDialogueStar(5);
                }
            }
        }
    }
}
