using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collectCoins : MonoBehaviour
{
    public GameObject beforePoint;
    public GameObject afterPoint;
    public GameObject money;


    // Start is called before the first frame update

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        MainPlayer playerUnit = hitInfo.GetComponent<MainPlayer>();
        if (hitInfo.gameObject.tag == "Player")
        {
            beforePoint.SetActive(false);
            afterPoint.SetActive(true);
            money.SetActive(false);
        }
    }

}
