using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps; 

public class crashWallLvl1 : MonoBehaviour
{
    public AudioClip[] effect;
    public int countCrash;

    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource audioEffectSource;
    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        if (hitInfo.gameObject.tag == "Collision")
        {
            countCrash++;
            audioEffectSource.Stop();
            audioEffectSource.clip = effect[0];
            audioEffectSource.Play();
            if (countCrash >= 4)
            {
                audioEffectSource.clip = effect[1];
                audioEffectSource.Play();
                countCrash = 0;
            }
        }
    }

}
