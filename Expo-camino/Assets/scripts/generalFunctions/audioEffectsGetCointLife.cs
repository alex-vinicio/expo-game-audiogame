using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioEffectsGetCointLife : MonoBehaviour
{

    public AudioClip[] listSounds;
    [Space]
    public AudioSource audioSource;
    public int[] durationAudio;

    public void activeSound(int indexListSound){//if index == 1 lose life if is 0 he win coin or if is 2 he restar level.
        if(indexListSound >= listSounds.Length){
            audioSource.Stop();
            return;
        }

        audioSource.Stop();
        audioSource.clip = listSounds[indexListSound];
        audioSource.Play();
    }

    public int getTimeDurationSoundEffect(int indexAudio){
        return durationAudio[indexAudio];
    }
}
