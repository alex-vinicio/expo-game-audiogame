using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class activeAnimationDoor : MonoBehaviour
{
    public int pointChurch;
    public GameObject cantu;
    //public GameObject papaOfChurchs;
    //variable for changue duration animation of the door
    public int[] timeAnimations;
    public int indexAnimationTimeArray;

    [Space]
    public AudioSource audioSource;
    // Start is called before the first frame update
    /*void Start()
    {
        rosaPoint2.SetActive(false);

    }*/
    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        MainPlayer playerUnit = hitInfo.GetComponent<MainPlayer>();
        if (hitInfo.gameObject.tag == "Player")
        {
            MainPlayerMovement player = playerUnit.GetComponent<MainPlayerMovement>();
            player.ActivateMoveControll(false, false, 0, 0);
            audioSource.Stop();
            FindObjectOfType<audioEffectDoors>().activeAnimationDoor();//active open door animation
            StartCoroutine(waitTimeAnimationDoor(timeAnimations, indexAnimationTimeArray));//wait a time duration of aniamtion open, that depend the numero of array of timeAnimations  
        }
    }

    IEnumerator waitTimeAnimationDoor(int[] timeAnimations, int indexAnimationTimeArray)
    {
        yield return new WaitForSeconds(timeAnimations[indexAnimationTimeArray]);
        if (pointChurch == 1)
        {
            FindObjectOfType<animationPanelChanguePositionPlayer>().asignPositionValues(-174.1f, 100.1f);//asing value of move position in church1
            FindObjectOfType<animationPanelChanguePositionPlayer>().activePanelEffectAnimation();
            cantu.SetActive(true);
        }
        else
        {
            if (pointChurch == 2)
            {
                //that is active for next church
            }
        }
    }
    public int[] valueTimeForDurationDoorAudio()
    {
        return timeAnimations;
    }



}
