using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changueBackGroundAudio : MonoBehaviour
{
    public AudioClip[] audioListBackGround;

    [Space]
    public AudioSource audioSource;

    public void changueAudioBackGround(int indexListAudio){
        if(indexListAudio < audioListBackGround.Length){//control for no play a index hight of vector audio
            audioSource.Stop();
            audioSource.clip = audioListBackGround[indexListAudio];
            audioSource.Play();
        }
    }
}
