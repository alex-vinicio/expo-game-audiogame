using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerIntroFirst : MonoBehaviour
{
    private DialogueManagerIntro dialogueManager;

    public AudioClip[] voices;
    public AudioClip[] punctuations;
    public int[] secondsAudios;
    public GameObject playerSoundIntro;
    public GameObject robotSound1;
    public GameObject mainPlayer;
    //private bool restriction = false;

    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;

    void Start()
    {
        dialogueManager = GetComponent<DialogueManagerIntro>();
    }

    public int[] valueTimeVoices(){//It use for add vector seconts in other class
        return secondsAudios;
    }

    public void ReproduceSound(int index){
        if(index >= secondsAudios.Length){
            voiceSource.Stop();
            StopAllCoroutines();
            StartCoroutine(waitForPauseVoice());//wait a 2s for pause audio and continue the dialogue
            return;
        }

        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }

    IEnumerator waitForPauseVoice(){
        yield return new WaitForSeconds(2);
        playerSoundIntro.SetActive(false);//use that for desactivate de game object finished and 
        robotSound1.SetActive(true);//activate a nuevo gameObject for reduce a resources in memori
        MainPlayer player = mainPlayer.GetComponent<MainPlayer>();
        Robot robot = robotSound1.GetComponent<Robot>();
        
        robot.activateDialogRobot1(player);
    }
}
