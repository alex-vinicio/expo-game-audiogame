using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot : MonoBehaviour
{
    public Dialogue dialogue;

    public void TriggerRobotNPC(MainPlayer playerUnit){
        FindObjectOfType<DialogueManagerIntro>().StartDialogueRobot(dialogue, playerUnit);
    }

    void OnTriggerEnter2D(Collider2D hitInfo){
        MainPlayer playerUnit =  hitInfo.GetComponent<MainPlayer>();

        if(hitInfo.gameObject.tag == "Player"){
            TriggerRobotNPC(playerUnit);
        }
    }

    public void activateDialogRobot1(MainPlayer playerUnit){
        TriggerRobotNPC(playerUnit);
    }
}
