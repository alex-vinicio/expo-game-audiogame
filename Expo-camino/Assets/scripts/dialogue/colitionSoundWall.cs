using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colitionSoundWall : MonoBehaviour
{
    public AudioSource effectImpactWall;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D hitinfo){
        if(hitinfo.gameObject.tag == "Player"){
            effectImpactWall.Play();
        }
    }
}
