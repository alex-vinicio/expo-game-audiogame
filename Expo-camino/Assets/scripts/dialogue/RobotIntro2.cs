using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotIntro2 : MonoBehaviour
{
     public Dialogue dialogue;

    public void TriggerRobotNPC(MainPlayer playerUnit){
        FindObjectOfType<DialogueManagerIntro>().StartDialogueIvibot2(dialogue, playerUnit);//asign correct function of classe DialogueManagerIntro
    }

    // void OnTriggerEnter2D(Collider2D hitInfo){
    //     MainPlayer playerUnit =  hitInfo.GetComponent<MainPlayer>();

    //     if(hitInfo.gameObject.tag == "Player"){
    //         TriggerRobotNPC(playerUnit);
    //     }
    // }

    public void activateDialogRobot2(MainPlayer playerUnit){
        TriggerRobotNPC(playerUnit);
    }
}
