using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;

public class ControlDialogueQuests : MonoBehaviour
{
    //public variables
    public TextMeshProUGUI dialogText;
    public GameObject canvasDialog;
    public GameObject buttonActionsDialog;

    private Queue<string> sentences;
    private string sentence;

    //private variables
    private int indexVoices,index=0;
    private int tipeNPCAudio;
    private int[] valueTimeVoice;
    private bool activeDialogue = false;

    void Start() {
        sentences = new Queue<string>();
    }

    void Update(){        
        if(activeDialogue){
            if(index < valueTimeVoice.Length){
                activeDialogue=false;
                StartCoroutine(activation(index));
            }
        }
    
        if(dialogText.text == sentence){
            if(index < (valueTimeVoice.Length-1))//active button next for one lesses index od vector dialogue
                buttonActionsDialog.SetActive(true);
        }
    }    

    IEnumerator activation(int index){
        yield return new WaitForSeconds(valueTimeVoice[index]);
        activeDialogue=true;
        DisplayNextStentence();
        buttonActionsDialog.SetActive(false);
    }

    //player dialogue 1
    public void StartDialogueNpc(Dialogue dialogue, MainPlayer playerUnit){
        index = 0;
        indexVoices =0;
        tipeNPCAudio = 1;
        valueTimeVoice = changueSoundDurationGeneral(tipeNPCAudio);
        canvasDialog.SetActive(true);//very important the first go gameObject or prefab active and lest continue animation
        
        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        activeDialogue = true;
        DisplayNextStentence();  
    }
    //Start
    public void StartDialogueNpcFirst(Dialogue dialogue, int point){
        index = 0;
        indexVoices =0;
        tipeNPCAudio = point;
        valueTimeVoice = changueSoundDurationGeneral(point);
        canvasDialog.SetActive(true);//very important the first go gameObject or prefab active and lest continue animation

        sentences.Clear();
        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        activeDialogue = true;
        DisplayNextStentence();  
    }

    public void ActiveNextStentence(bool activeDialog){
        activeDialogue = activeDialog;
        DisplayNextStentence();
    }

    public void DisplayNextStentence(){
        if(sentences.Count == 0){
            EndDialogue();
            index = 0;
            return;
        }
        sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    public int[] changueSoundDurationGeneral(int tipeSound){
        if(tipeSound == 1){
            if(FindObjectOfType<startQuest1OptionalLvl2>())
                return  FindObjectOfType<startQuest1OptionalLvl2>().addIndexVoice();
            else{
                return null;
            }
        }else{
            if(tipeSound == 2){
                if(FindObjectOfType<voicesParagraphRosa>()){
                    return  FindObjectOfType<voicesParagraphRosa>().addIndexVoice();
                }else{
                    return null;
                }
            }else{
                if(tipeSound == 4){
                    return  FindObjectOfType<audioDialgueRosaLvl2>().addIndexVoice();
                }else{
                    if(tipeSound == 5){
                        return  FindObjectOfType<audioDialgueRosaLvl2>().addIndexVoice();
                    }else{
                        if(tipeSound == 6){
                            return FindObjectOfType<pastorsNPCAudioControl>().addIndexVoice();
                        }else{
                            if(tipeSound == 7){
                                return FindObjectOfType<pastorsNPCAudioControl>().addIndexVoice();
                            }else{
                               if(tipeSound == 8){
                                    return FindObjectOfType<pastorsNPCAudioControl>().addIndexVoice();
                                }else{
                                    if(tipeSound == 9){
                                        return FindObjectOfType<pastorsNPCAudioControl>().addIndexVoice();
                                    }else{
                                        if(tipeSound == 10){
                                            return FindObjectOfType<pastorsNPCAudioControl>().addIndexVoice();
                                        }else{
                                            if(tipeSound == 11){
                                                return FindObjectOfType<pastorsNPCAudioControl>().addIndexVoice();
                                            }else{
                                                if(tipeSound == 12){
                                                    return FindObjectOfType<audioDialogueJoseLvl2>().addIndexVoice();
                                                }else{
                                                    return null;
                                                }
                                            }
                                        }
                                    }
                                } 
                            }
                        }
                    }
                }
            }
        }
    }

    public void changueSoundActivationGeneral(int tipeSound){
        if(tipeSound == 1){
            if(FindObjectOfType<startQuest1OptionalLvl2>())
                FindObjectOfType<startQuest1OptionalLvl2>().ReproduceSound(indexVoices);
        }else{
            if(tipeSound == 2){
                if(FindObjectOfType<voicesParagraphRosa>())
                    FindObjectOfType<voicesParagraphRosa>().ReproduceSound(indexVoices);
            }else{
                if(tipeSound == 4){
                    if(FindObjectOfType<audioDialgueRosaLvl2>())
                        FindObjectOfType<audioDialgueRosaLvl2>().ReproduceSound(indexVoices);
                }else{
                    if(tipeSound == 5){
                        if(FindObjectOfType<audioDialgueRosaLvl2>())
                            FindObjectOfType<audioDialgueRosaLvl2>().ReproduceSound(indexVoices);
                    } else{
                        if(tipeSound == 6){
                            if(FindObjectOfType<pastorsNPCAudioControl>())
                                FindObjectOfType<pastorsNPCAudioControl>().ReproduceSound(indexVoices);
                        }else{
                            if(tipeSound == 7){
                                if(FindObjectOfType<pastorsNPCAudioControl>())
                                    FindObjectOfType<pastorsNPCAudioControl>().ReproduceSound(indexVoices);
                            }else{
                                if(tipeSound == 8){
                                    if(FindObjectOfType<pastorsNPCAudioControl>())
                                        FindObjectOfType<pastorsNPCAudioControl>().ReproduceSound(indexVoices);
                                }else{
                                    if(tipeSound == 9){
                                        if(FindObjectOfType<pastorsNPCAudioControl>())
                                            FindObjectOfType<pastorsNPCAudioControl>().ReproduceSound(indexVoices);
                                    }else{
                                        if(tipeSound == 10){
                                            if(FindObjectOfType<pastorsNPCAudioControl>())
                                                FindObjectOfType<pastorsNPCAudioControl>().ReproduceSound(indexVoices);
                                        }else{
                                            if(tipeSound == 11){
                                                if(FindObjectOfType<pastorsNPCAudioControl>())
                                                    FindObjectOfType<pastorsNPCAudioControl>().ReproduceSound(indexVoices);
                                            }else{
                                                if(tipeSound == 12){
                                                    if(FindObjectOfType<audioDialogueJoseLvl2>())
                                                        FindObjectOfType<audioDialogueJoseLvl2>().ReproduceSound(indexVoices);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    IEnumerator TypeSentence(string sentence){
        dialogText.text = "";
        buttonActionsDialog.SetActive(false);
        changueSoundActivationGeneral(tipeNPCAudio);
        indexVoices++;
        foreach (char letter in sentence.ToCharArray())
        {
             dialogText.text += letter; //important add + for add in the TMP letter for letter
             yield return null;
        }
        index++; //count for control of index of durations audio vectors
    }

    void EndDialogue(){
        canvasDialog.SetActive(false);
        changueSoundActivationGeneral(tipeNPCAudio);
        activeDialogue=false;
    }
}
