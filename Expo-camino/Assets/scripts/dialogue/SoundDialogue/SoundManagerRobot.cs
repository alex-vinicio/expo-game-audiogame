using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerRobot : MonoBehaviour
{
    private DialogueManagerIntro dialogueManager;

    public AudioClip[] voices;
    public AudioClip[] punctuations;
    public int[] secondsAudios;
    public GameObject robotIntro1;
    public GameObject robotIntro2;
    public GameObject mainPlayer;
    public AudioSource tvAudio;
    //private bool restriction = false;
    public int timePauseDialogue;

    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;

    void Start()
    {
        dialogueManager = GetComponent<DialogueManagerIntro>();
    }

    public int[] valueTimeVoices(){//It use for add vector seconts in other class
        return secondsAudios;
    }

    public void ReproduceSound(int index){
        if(index >= secondsAudios.Length){
            voiceSource.Stop();
            StopAllCoroutines();
            StartCoroutine(waitForPauseVoice());
            return;
        }

        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }

    IEnumerator waitForPauseVoice(){
        yield return new WaitForSeconds(timePauseDialogue);
        robotIntro1.SetActive(false);//use that for desactivate de game object finished and 
        robotIntro2.SetActive(true);//activate a nuevo gameObject for reduce a resources in memori
        tvAudio.Stop();
        MainPlayer player = mainPlayer.GetComponent<MainPlayer>();
        RobotIntro2 robot2 = robotIntro2.GetComponent<RobotIntro2>();
        robot2.activateDialogRobot2(player); //activate a function for continue dialogue
    }
}
