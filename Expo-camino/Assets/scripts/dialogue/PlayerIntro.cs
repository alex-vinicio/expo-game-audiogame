using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIntro : MonoBehaviour
{
    public Dialogue dialogue;
    public float timeWaitIntroPlayer = 5f;
    public MainPlayer player;

    void Start(){
        if(player){//load all resources inf the scene and wait 3s for generate the dialogue
            StartCoroutine(activeDuration() );
        }
    }

    IEnumerator activeDuration(){
        yield return new WaitForSeconds(timeWaitIntroPlayer);
        TriggerCowNPC(player);
    }

    public void TriggerCowNPC(MainPlayer playerUnit){
        FindObjectOfType<DialogueManagerIntro>().StartDialogueCow(dialogue, playerUnit);
    }

}
