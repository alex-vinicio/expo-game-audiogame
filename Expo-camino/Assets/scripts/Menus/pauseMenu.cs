using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pauseMenu : MonoBehaviour
{
    public static bool gameIsPaused = false;

    public GameObject pauseMenuUI;
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)){
            if(gameIsPaused){
                resumeGame();
            }else{
                pauseGame();
            }
        }
        
    }

    public void resumeGame(){  
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    void pauseGame(){
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }
    //example of change audio if we use pause mode
    /*if(PauseMenu.gameIsPaused){
        s.source.pitch *= .5f;
    }*/

    public void redirectionMenu(){
        Time.timeScale = 1f;//usado para quitar el pausa en cualquier animacion
        SceneManager.LoadScene("Main menu");
    }
}
