using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotIntro5 : MonoBehaviour
{
    public Dialogue dialogue;

    public void TriggerRobotNPC(MainPlayer playerUnit){
        FindObjectOfType<DialogueManagerIntro>().StartDialogueIvibot2(dialogue, playerUnit,4);//asign correct function of classe DialogueManagerIntro
    }
    
    public void activateDialogRobot5(MainPlayer playerUnit){
        TriggerRobotNPC(playerUnit);
    }
}
