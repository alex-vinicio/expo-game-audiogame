using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundManagerIntroFirst6 : MonoBehaviour
{
     private DialogueManagerIntro dialogueManager;

    public AudioClip[] voices;
    public int[] secondsAudios;
    public GameObject robotIntro1;
    public GameObject mainPlayer;
    //private bool restriction = false;
    public int timePauseDialogue;

    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;

    void Start()
    {
        dialogueManager = GetComponent<DialogueManagerIntro>();
    }

    public int[] valueTimeVoices(){//It use for add vector seconts in other class
        return secondsAudios;
    }

    public void ReproduceSound(int index){
        if(index >= secondsAudios.Length){
            voiceSource.Stop();
            StopAllCoroutines();
            StartCoroutine(waitForPauseVoice());
            return;
        }

        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }

    IEnumerator waitForPauseVoice(){
        yield return new WaitForSeconds(timePauseDialogue);
        robotIntro1.SetActive(false);//use that for desactivate de game object finished and 
        SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);
    }
}
