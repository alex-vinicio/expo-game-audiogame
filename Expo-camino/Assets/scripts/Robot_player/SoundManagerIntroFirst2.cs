using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerIntroFirst2 : MonoBehaviour
{
    private DialogueManagerIntro dialogueManager;

    public AudioClip[] voices;
    public int[] secondsAudios;
    public GameObject robotNow;
    public GameObject robotNext;
    public GameObject mainPlayer;
    //acces to points o references
    public GameObject point1;

    public AudioSource referencePoint1;
    public int timePauseDialogue;

    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;

    void Start()
    {
        dialogueManager = GetComponent<DialogueManagerIntro>();
        point1.SetActive(true);
    }

    public int[] valueTimeVoices(){//It use for add vector seconts in other class
        return secondsAudios;
    }

    public void ReproduceSound(int index){
        if(index >= secondsAudios.Length){
            voiceSource.Stop();
            StartCoroutine(waitForPauseVoice());
            return;
        }
        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }

    IEnumerator waitForPauseVoice(){
        
        yield return new WaitForSeconds(timePauseDialogue);
        referencePoint1.Play();
        robotNow.SetActive(false);//use that for desactivate de game object finished and 
        robotNext.SetActive(true);//activate a nuevo gameObject for reduce a resources in memori
        MainPlayerMovement player = mainPlayer.GetComponent<MainPlayerMovement>();
        player.ActivateMoveControll(true, true, 1, 0);
    }
}
