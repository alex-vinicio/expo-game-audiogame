using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class robotIntro3 : MonoBehaviour
{
    public Dialogue dialogue;

    public void TriggerRobotNPC(MainPlayer playerUnit){
        FindObjectOfType<DialogueManagerIntro>().StartDialogueIvibot2(dialogue, playerUnit,2);//asign correct function of classe DialogueManagerIntro
    }
    
    public void activateDialogRobot3(MainPlayer playerUnit){
        TriggerRobotNPC(playerUnit);
    }
}
