using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotIntro2 : MonoBehaviour
{
     public Dialogue dialogue;

    public void TriggerRobotNPC(MainPlayer playerUnit){
        FindObjectOfType<DialogueManagerIntro>().StartDialogueIvibot2(dialogue, playerUnit,1);//asign correct function of classe DialogueManagerIntro
    }
    
    public void activateDialogRobot2(MainPlayer playerUnit){
        TriggerRobotNPC(playerUnit);
    }
}
