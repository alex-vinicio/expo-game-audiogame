using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioPlayControll4 : MonoBehaviour
{
    public AudioSource sonidoCampana;
    public GameObject pointReferences;
    public GameObject robotNext;

    void OnTriggerEnter2D(Collider2D hitInfo){
        MainPlayer playerUnit =  hitInfo.GetComponent<MainPlayer>();

        if(hitInfo.gameObject.tag == "Player"){
            sonidoCampana.Stop();
            MainPlayerMovement player = hitInfo.GetComponent<MainPlayerMovement>();
            player.ActivateMoveControll(false, false, 0, 0);
            
            RobotIntro6 robot = robotNext.GetComponent<RobotIntro6>();
            robot.activateDialogRobot6(playerUnit);

            pointReferences.SetActive(false);
        }
    }
}
