using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioPlayControll3 : MonoBehaviour
{
    public AudioSource sonidoCampana;
    public GameObject pointReferences;
    public GameObject robotNext;
    public GameObject pointNext;
    public AudioSource sonidoCampanaNext;

    void OnTriggerEnter2D(Collider2D hitInfo){
        MainPlayer playerUnit =  hitInfo.GetComponent<MainPlayer>();

        if(hitInfo.gameObject.tag == "Player"){
            sonidoCampana.Stop();
            MainPlayerMovement player = hitInfo.GetComponent<MainPlayerMovement>();
            player.ActivateMoveControll(false, false, 0, 0);
            
            RobotIntro5 robot = robotNext.GetComponent<RobotIntro5>();
            robot.activateDialogRobot5(playerUnit);

            pointNext.SetActive(true);
            sonidoCampanaNext.Play();
            pointReferences.SetActive(false);
        }
    }
}
