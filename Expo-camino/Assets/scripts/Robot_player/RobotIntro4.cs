using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotIntro4 : MonoBehaviour
{
    public Dialogue dialogue;

    public void TriggerRobotNPC(MainPlayer playerUnit){
        FindObjectOfType<DialogueManagerIntro>().StartDialogueIvibot2(dialogue, playerUnit,3);//asign correct function of classe DialogueManagerIntro
    }
    
    public void activateDialogRobot4(MainPlayer playerUnit){
        TriggerRobotNPC(playerUnit);
    }
}
