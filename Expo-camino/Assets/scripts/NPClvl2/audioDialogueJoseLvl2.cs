using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioDialogueJoseLvl2 : MonoBehaviour
{
   private DialogueManagerIntro dialogueManager;

    public AudioClip[] voices;
    public int[] secondsAudios;

    public GameObject mainPlayer;
    //acces to points o references
    public int timePauseDialogue;
    public int orderJoseNpc;
    public GameObject canvasQuestions;
    public GameObject nextPointObject;
    public GameObject generateDialoguePoint;

    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;

    void Start()
    {
        dialogueManager = GetComponent<DialogueManagerIntro>();
    }

    //It use for add vector seconts in other class
    public int[] addIndexVoice(){
        return secondsAudios;
    }
    public void ReproduceSound(int index){
        if(index >= secondsAudios.Length){
            StopAllCoroutines();
            voiceSource.Stop();
            StartCoroutine(waitForPauseVoice());
            return;
        }
        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }

    IEnumerator waitForPauseVoice(){
        yield return new WaitForSeconds(timePauseDialogue);

        if(orderJoseNpc == 0){
            MainPlayerMovement player = mainPlayer.GetComponent<MainPlayerMovement>();
            player.ActivateMoveControll(true, false, 0, 0);
            nextPointObject.SetActive(true);
        }else{
            if(orderJoseNpc == 3){
                MainPlayerMovement player = mainPlayer.GetComponent<MainPlayerMovement>();
                player.ActivateMoveControll(false, false, 0, 0);
                FindObjectOfType<animationPanelChanguePositionPlayer>().asignPositionValues(384.4f, 212.1f);//asing value of move position in church1
                FindObjectOfType<animationPanelChanguePositionPlayer>().activePanelEffectAnimation();
                nextPointObject.SetActive(true);
            }else{
                if(orderJoseNpc == 4){
                    canvasQuestions.SetActive(true);
                    FindObjectOfType<moveSelection>().startPosition();//restar the position of selecction of the answer's value
                    generateQuestionForFinalChurch qp = generateDialoguePoint.GetComponent<generateQuestionForFinalChurch>();
                    qp.activeQuestionsP1Lvl2();//active function of questions general
                }else{
                    if(orderJoseNpc == 5){
                        FindObjectOfType<posibleScriptFinish>().FinishToLevel();
                    }else{
                        canvasQuestions.SetActive(true);
                        FindObjectOfType<moveSelection>().startPosition();//restar the position of selecction of the answer's value
                        questionsPointLvl2 qp = generateDialoguePoint.GetComponent<questionsPointLvl2>();
                        qp.activeQuestionsP1Lvl2();//active function of questions general
                    }
                }
            }
        }
    }
}
