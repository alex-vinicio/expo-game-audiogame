using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioEffectDoors : MonoBehaviour
{
    public AudioClip[] audioDoorOpens;
    public AudioClip[] audioDoorClose;
    public Animator animatorDoor;

    [Space]
    public AudioSource openDoor;
    // Start is called before the first frame update
   
   public void startEffectSountDoor(int indexSound){
       openDoor.Stop();
       openDoor.clip = audioDoorOpens[indexSound];
       openDoor.Play();
   }

   public void closeEffectSoundDoor(int indexSound){
       openDoor.Stop();
       openDoor.clip = audioDoorClose[indexSound];
       openDoor.Play();
   }

    public void activeStateOpenDoor(){
        animatorDoor.SetBool("onOpen", true);
        animatorDoor.SetBool("waitOpen", true);
    }

    public void activeStateCloseDoor(){
        animatorDoor.SetBool("onOpen", false);
        animatorDoor.SetBool("waitOpen", false);
    }

    public void activeAnimationDoor(){
        animatorDoor.SetBool("onOpen", true);
        animatorDoor.SetBool("waitOpen", false);
    }

    public void activeFirstStateCloseDoor(){
        animatorDoor.SetBool("onOpen", false);
        animatorDoor.SetBool("waitOpen", true);
    }
}
