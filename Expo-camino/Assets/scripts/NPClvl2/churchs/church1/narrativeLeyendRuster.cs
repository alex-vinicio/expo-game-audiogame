using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class narrativeLeyendRuster : MonoBehaviour
{
    public AudioClip[] lenyendClips;

    [Space]
    public AudioSource audioSource;
    
    public GameObject beforePointChurch1;
    public GameObject nextPointChurch1;
    public int timeDurationLeyendChurch1;

    // Start is called before the first frame update
    void Start()
    {
        beforePointChurch1.SetActive(false);
        audioSource.Stop();
        audioSource.clip = lenyendClips[0];
        audioSource.Play();
        FindObjectOfType<activePanelRuster>().activeAnimation();
        StartCoroutine(timeDurationLeyend());
    }
    IEnumerator timeDurationLeyend(){
        yield return new WaitForSeconds(timeDurationLeyendChurch1);
        nextPointChurch1.SetActive(true);
    }
}
