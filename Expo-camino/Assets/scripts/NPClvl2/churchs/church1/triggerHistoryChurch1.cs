using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerHistoryChurch1 : MonoBehaviour
{
    public int pointBook;
    public AudioSource audioSourcePointChurch;

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        MainPlayer playerUnit =  hitInfo.GetComponent<MainPlayer>();
        if(hitInfo.gameObject.tag == "Player"){
            audioSourcePointChurch.loop = false;
            MainPlayerMovement player = playerUnit.GetComponent<MainPlayerMovement>();
            player.ActivateMoveControll(false, false, 0, 0);
            if(pointBook == 1){
                FindObjectOfType<pastorsNPCTexts>().activateDialoguePastorChurchs(7);
            }else{
                if(pointBook == 2){
                    FindObjectOfType<pastorsNPCTexts>().activateDialoguePastorChurchs(8);
                }else{
                    if(pointBook == 3){
                        FindObjectOfType<pastorsNPCTexts>().activateDialoguePastorChurchs(9);
                    }else{
                        if(pointBook == 4){
                            FindObjectOfType<pastorsNPCTexts>().activateDialoguePastorChurchs(10);
                        }else{
                            if(pointBook == 5){
                                FindObjectOfType<pastorsNPCTexts>().activateDialoguePastorChurchs(11);
                            }
                        }
                    }
                }
            }
        }
    }
}
