using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Globalization;

public class posibleScriptFinish : MonoBehaviour
{
    [Space]
    public AudioSource audio;
    public int timeWaitAudio;
    public int levelUpdate;

    public void FinishToLevel(){
        FindObjectOfType<updateValueLvl2>().saveWinSscoreVariableGeneral(0);
        StartCoroutine(verifyConectServer());   
    }
    IEnumerator waitAudio(){
        audio.Stop();
        audio.Play();
        yield return new WaitForSeconds(timeWaitAudio);
        FindObjectOfType<levelLoad>().LoadLevel(1);
    }
    IEnumerator verifyConectServer(){
        UnityWebRequest www = UnityWebRequest.Get("http://expocamino.azurewebsites.net/validateInternetConection/serverOn.php");//URL of service registered
        yield return www.SendWebRequest();//add new tecnologi replace a WWW obsolet
        switch (www.result)
        {
            case UnityWebRequest.Result.ConnectionError:
                StartCoroutine(waitAudio());
                break;
            case UnityWebRequest.Result.DataProcessingError:
                StartCoroutine(waitAudio());
                break;
            case UnityWebRequest.Result.ProtocolError:
                StartCoroutine(waitAudio());
                break;
            case UnityWebRequest.Result.Success:
                StartCoroutine(chargeDataPlayer());
                break;
        }
    }
    IEnumerator chargeDataPlayer(){
        WWWForm formU= new WWWForm();//create a form for sent Post of server
        formU.AddField("name", playerData.namePlayer);
        formU.AddField("idLevel", levelUpdate);
        formU.AddField("ckX", "18");
        formU.AddField("ckY", "212.4");
        formU.AddField("time", FindObjectOfType<updateValueLvl2>().getTimeLevel());
        formU.AddField("numRepeat", FindObjectOfType<MainPlayer>().TakeDamage(0));
        formU.AddField("pointsWin", playerData.score);
        formU.AddField("answerFails", playerData.respuestasFallidas);
        formU.AddField("distanceWalk", "0");
        formU.AddField("state", "0");
        UnityWebRequest www = UnityWebRequest.Post("http://expocamino.azurewebsites.net/levelquest/setDataLevel.php", formU);//URL of service registered
        yield return www.SendWebRequest();//add new tecnologi replace a WWW obsolet
        if(www.downloadHandler.text[0] == '0'){//to assign variable player to charge in player
            playerData.positionX =  0.0f;
            playerData.positionY = 0.0f;
            playerData.tiempoNivel = 0.0f;
            playerData.respuestasFallidas = 0;
            playerData.distanceRoad = 0;
            playerData.nivelActual = 3;
            playerData.numRepeticion = FindObjectOfType<MainPlayer>().TakeDamage(0);

            StartCoroutine(waitAudio());
        }
    }
}
