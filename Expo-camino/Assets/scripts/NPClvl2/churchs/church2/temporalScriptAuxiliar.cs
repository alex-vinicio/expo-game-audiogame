using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class temporalScriptAuxiliar : MonoBehaviour
{
    public GameObject nextPoint;
    public GameObject beforePoint;
    
    public void temporalWaitTime(){
        beforePoint.SetActive(false);
        StartCoroutine(waitTime());
    }
    IEnumerator waitTime(){
        yield return new WaitForSeconds(3);
        FindObjectOfType<moveSelection>().startPosition();//restar the position of selecction of the answer's value
        FindObjectOfType<updateValueLvl2>().scoreValuePlayer(12);
        nextPoint.SetActive(true);
    }
}
