using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dialogueTextQuestO1 : MonoBehaviour
{
    public Dialogue dialogue;
    public string[] extraText;

    public void TriggerNPCQuest(MainPlayer playerUnit){

        FindObjectOfType<ControlDialogueQuests>().StartDialogueNpc(dialogue, playerUnit);//asign correct function of classe DialogueManagerIntro
    }
}
