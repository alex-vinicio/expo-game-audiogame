using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changueAudioCat : MonoBehaviour
{
    public AudioClip[] voices;
    public Transform rbc;
    public float valueMoveX;
    public float valueMoveY;
    private bool move = false;
    public CircleCollider2D circleColiderC;

    Vector2 movement;

    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;


    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        MainPlayer playerUnit =  hitInfo.GetComponent<MainPlayer>();
        if(hitInfo.gameObject.tag == "Player"){
            move = true;
            MainPlayerMovement player = playerUnit.GetComponent<MainPlayerMovement>();
            FindObjectOfType<startQuest1OptionalLvl2>().specificSoundByIndex(3);
            FindObjectOfType<startQuest1OptionalLvl2>().returnPlayerQuest();
            voiceSource.Stop();
            voiceSource.clip = voices[1];
            voiceSource.Play();

            movement.x =  54.72246f;
            movement.y =  -0.7151992f;
            Destroy(circleColiderC);
        }
    }

    void FixedUpdate(){
        if(move){
            if((movement.x >= 71.66012f) && (movement.y >= 31.97767f)){
                movement.x = 71.66012f;
                movement.y = 31.97767f;
                rbc.position = movement;//add value at position whit value of movement and speed 
            }else{
                movement.x = movement.x +valueMoveX* Time.deltaTime;
                movement.y = movement.y +valueMoveY * Time.deltaTime;
                rbc.position = movement;//add value at position whit value of movement and speed 
            }
        }
    }

    public void firstSoundCat(int index){
        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }
}
