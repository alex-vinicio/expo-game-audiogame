using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class questionsPointLvl2 : MonoBehaviour
{
    [TextArea(1,20)]
    public string[] quest;
    public int[] answerValue;

    public AudioClip[] audioQuest;
    public int[] audioDuration;

    public AudioClip[] valueRandomFalse;
    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;
    [Space]
    public AudioSource voiceSourceAllAnswers;
    //alert lose lifes and restar level
    public AudioClip[] alertRestarLevelAudio;
    //game objects
    public GameObject canvasPlayerDashboad;
    public GameObject nextPoint;
    public GameObject canvasQuestion;
    public TextMeshProUGUI questText;
    public TextMeshProUGUI answer1;
    public TextMeshProUGUI answer2;
    public TextMeshProUGUI answer3;
    //values controllers in oportunities
    public int numberOfFailsForLessesLife;
    public int loadLevelIfLose;
    public int numMaxOfQuestions;
    //private variable
    private bool changueOptions;
    private int countPressButton;
    private int indexVector;
    private int indexNextVector;
    private int valueFail1, valueFail2;
    private int randomValueVerificate;
    private int[] randomPositionAnswer;
    private int numberErrorSubstract;
    private int numberOfQuestion;
    private int beforeValueOptions;

    void Start(){
        beforeValueOptions = 100;
        numberOfQuestion = 1;
        numberErrorSubstract = 1;
        valueFail1 = 0;
        valueFail2 = 0;
        countPressButton = 0;
        changueOptions = false;
    }

    public void setTextAnswer(int indexVector,int valueFail1, int valueFail2){
        randomPositionAnswer = randomVectorAnswer(answerValue[indexVector], valueFail1, valueFail2);//generate a random position of answer in a array

        answer1.text= randomPositionAnswer[0].ToString();
        answer2.text= randomPositionAnswer[1].ToString();
        answer3.text= randomPositionAnswer[2].ToString();
    }

    public int[] randomVectorAnswer(int valueIndex1, int valueIndex2, int valueIndex3){
        int[] vectorStoreValueInText = new int[3];

        randomValueVerificate = Random.Range(0,5);

        if(randomValueVerificate == 0){
            vectorStoreValueInText[0] = valueIndex1;
            vectorStoreValueInText[1] = valueIndex2;
            vectorStoreValueInText[2] = valueIndex3;
            return vectorStoreValueInText;
        }else{
            if(randomValueVerificate == 1){
                vectorStoreValueInText[0] = valueIndex2;
                vectorStoreValueInText[1] = valueIndex1;
                vectorStoreValueInText[2] = valueIndex3;
                return vectorStoreValueInText;
            }else{
                if(randomValueVerificate == 2){
                    vectorStoreValueInText[0] = valueIndex3;
                    vectorStoreValueInText[1] = valueIndex2;
                    vectorStoreValueInText[2] = valueIndex1;
                    return vectorStoreValueInText;
                }else{
                    if(randomValueVerificate == 3){
                        vectorStoreValueInText[0] = valueIndex2;
                        vectorStoreValueInText[1] = valueIndex3;
                        vectorStoreValueInText[2] = valueIndex1;
                        return vectorStoreValueInText;
                    }else{
                        if(randomValueVerificate == 4){
                            vectorStoreValueInText[0] = valueIndex3;
                            vectorStoreValueInText[1] = valueIndex1;
                            vectorStoreValueInText[2] = valueIndex2;
                            return vectorStoreValueInText;
                        }else{
                            vectorStoreValueInText[0] = valueIndex1;
                            vectorStoreValueInText[1] = valueIndex3;
                            vectorStoreValueInText[2] = valueIndex2;
                            return vectorStoreValueInText;
                        }
                    }
                }
            }
        }
    }

    public void soundClasifyPosition(int valuePositionPointer){//clasify the position in one of the three options and sent index audio
        if(valuePositionPointer == 0){
            ReproduceSound(randomPositionAnswer[0],2);
        }else{
            if(valuePositionPointer == 1){
                ReproduceSound(randomPositionAnswer[1],2);
            }else{
                if(valuePositionPointer == 2){
                    ReproduceSound(randomPositionAnswer[2],2);
                }   
            }
        }
    }
    void Update(){
        if(changueOptions){
            if (Input.GetKeyDown("d") || Input.GetKeyDown("right"))
            {
                if(countPressButton < 2){
                    countPressButton++;
                    FindObjectOfType<moveSelection>().rightPosition();//move a selector line in the options
                    soundClasifyPosition(countPressButton); //active audio for respective options
                }
            }
            if(Input.GetKeyDown("a") || Input.GetKeyDown("left")){
                if(countPressButton > 0){
                    countPressButton--;
                    FindObjectOfType<moveSelection>().leftPosition();
                    soundClasifyPosition(countPressButton);
                }
            }

            if(Input.GetKeyDown(KeyCode.Return)){
                if(countPressButton >= 0){
                    voiceSource.Stop();  
                    checkValueSelectAnswer(countPressButton, answerValue[indexVector]);
                }
            }
        }
    }
    public void checkValueSelectAnswer(int positionSelected, int correctAnswerValue){
        if(positionSelected == 0){
            addOrSubstractpoints(randomPositionAnswer[0], correctAnswerValue);
        }else{
            if(positionSelected == 1){
                addOrSubstractpoints(randomPositionAnswer[1], correctAnswerValue);
            }else{
                if(positionSelected == 2){
                    addOrSubstractpoints(randomPositionAnswer[2], correctAnswerValue);
                }   
            }
        }
    }
    public void addOrSubstractpoints(int valueSelected, int correctAnswerValue){
        print(numberOfQuestion);
        changueOptions=false;
        if(valueSelected == correctAnswerValue){
            FindObjectOfType<updateValueLvl2>().scoreValuePlayer(5); // saver value in temporal variable
            if(numberOfQuestion >= numMaxOfQuestions){
                canvasQuestion.SetActive(false);
                if(nextPoint){
                    nextPoint.SetActive(true);
                }
                FindObjectOfType<audioEffectsGetCointLife>().activeSound(5);//active audio effect of win
                FindObjectOfType<MainPlayerMovement>().ActivateMoveControll(true,false,0,0);//ability the move of player
                StartCoroutine(messageOfLoseLifesAndRestarLevel(FindObjectOfType<audioEffectsGetCointLife>().getTimeDurationSoundEffect(5),3));//get value of duration of audio effect
            }else{
                numberOfQuestion++;
                countPressButton = 0;
                FindObjectOfType<moveSelection>().startPosition();//restar the position of selecction of the answer's value
                FindObjectOfType<audioEffectsGetCointLife>().activeSound(4);
                StartCoroutine(messageOfLoseLifesAndRestarLevel(FindObjectOfType<audioEffectsGetCointLife>().getTimeDurationSoundEffect(4),2));
            }
        }else{
            FindObjectOfType<MainPlayer>().errorUpdate(1);//set a 1 error for count in the general variable
            if(numberErrorSubstract <=0 ){
                int lifeOfPlayer = FindObjectOfType<MainPlayer>().TakeDamage(1);//lesses a one point of life of the player
                FindObjectOfType<updateValueLvl2>().lifePointPlayer(lifeOfPlayer);//rechargue text of life
                FindObjectOfType<updateValueLvl2>().saveLessesScoreVariableGeneral((-1));//save value in general varaible

                if(FindObjectOfType<MainPlayer>().lifeValidate()){
                    //add component of restar the level, voice alert
                    FindObjectOfType<audioEffectsGetCointLife>().activeSound(2);
                    StartCoroutine(messageOfLoseLifesAndRestarLevel(FindObjectOfType<audioEffectsGetCointLife>().getTimeDurationSoundEffect(2),0));
                }else{
                    numberErrorSubstract = numberOfFailsForLessesLife;
                    FindObjectOfType<audioEffectsGetCointLife>().activeSound(1);
                    StartCoroutine(messageOfLoseLifesAndRestarLevel(FindObjectOfType<audioEffectsGetCointLife>().getTimeDurationSoundEffect(1),1));
                }
            }else{
                changueOptions = true;
                numberErrorSubstract--;
            }
        }
    }
    IEnumerator messageOfLoseLifesAndRestarLevel(int timeWaitRestarLevel,int destionAfterAnswer){//duration in seconds of lose life
        yield return new WaitForSeconds(timeWaitRestarLevel);
        if(destionAfterAnswer == 1){
            ReproduceSound(indexVector,1);
            changueOptions = true;
        }else{
            if(destionAfterAnswer == 0){
                canvasPlayerDashboad.SetActive(false);
                FindObjectOfType<levelLoad>().LoadLevel(loadLevelIfLose);
            }else{
                if(destionAfterAnswer == 2){
                    activeQuestionsP1Lvl2();
                }else{
                    if(destionAfterAnswer == 3){
                        FindObjectOfType<MainPlayerMovement>().ActivateMoveControll(true,false,0,0);//ability the move of player
                    }
                }
            }
        }
    }
    public void activeQuestionsP1Lvl2(){
        numberErrorSubstract = numberOfFailsForLessesLife;
        indexVector = Random.Range(0,quest.Length);//asign a rando value of question for 
        //section for the control of it isn´t repeated
        valueFail1 = Random.Range(0,valueRandomFalse.Length);
        valueFail2 = Random.Range(0,valueRandomFalse.Length);

        if(answerValue[indexVector] == beforeValueOptions){//if repeat the question before, reload the question for other random  
           activeQuestionsP1Lvl2(); 
        }
        verifyRepetitionsValuesAnswers(); 

        questText.text = quest[indexVector];
        ReproduceSound(indexVector,1);
        setTextAnswer(indexVector,valueFail1, valueFail2); //generate random values results and it's the input in TMPro
        StartCoroutine(startQuestions(indexVector));//time for wait a dialogue quest
    }

    IEnumerator startQuestions(int countPressButton){
        yield return new WaitForSeconds(audioDuration[countPressButton]);
        beforeValueOptions = answerValue[indexVector];
        
        soundClasifyPosition(0);
        changueOptions=true;//activate button move results selectionschangueOptions=true;//activate button move results selections
    }
    public void generateAleatoryValueFalse(){
        if(valueFail1 == valueFail2){
            valueFail1 = Random.Range(0,valueRandomFalse.Length);
            valueFail2 = Random.Range(0,valueRandomFalse.Length);
        }
    }
    public void verifyRepetitionsValuesAnswers(){
        generateAleatoryValueFalse();
        if(answerValue[indexVector] == valueFail1){
            valueFail1 = Random.Range(0,valueRandomFalse.Length);
        }else{
            if(answerValue[indexVector] == valueFail2){
                valueFail2 = Random.Range(0,valueRandomFalse.Length);
            }
        }
        generateAleatoryValueFalse();
    }
    public void ReproduceSound(int index, int typeVectorAudio){
        if(typeVectorAudio == 1){
            voiceSource.Stop();
            voiceSource.clip = audioQuest[index];
            voiceSource.Play();
        }else{
            if(typeVectorAudio == 2){
                voiceSourceAllAnswers.Stop();
                voiceSourceAllAnswers.clip = valueRandomFalse[index];
                voiceSourceAllAnswers.Play();
            }else{
                if(typeVectorAudio == 3){
                    voiceSource.Stop();
                    voiceSource.clip = alertRestarLevelAudio[index];
                    voiceSource.Play();
                }else
                {
                    return;
                }
            }
        }
    }
}
