using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dialoguePoint1Lvl2 : MonoBehaviour
{
    public Dialogue dialogue;
    public GameObject ivibotStartPoint;
    public GameObject rosaPoint1;
    public int point;

    void Start(){
        if(point == 1){
            ivibotStartPoint.SetActive(false);
        }else{
            if(point == 2){
                rosaPoint1.SetActive(false);
            }
        }
    }

    public void TriggerRosaNPC(int numActive){
        FindObjectOfType<ControlDialogueQuests>().StartDialogueNpcFirst(dialogue, numActive);//asign correct function of classe DialogueManagerIntro
    }
    
    public void activateDialogueRosaP1(int numActive){
        TriggerRosaNPC(numActive);
    }
}
