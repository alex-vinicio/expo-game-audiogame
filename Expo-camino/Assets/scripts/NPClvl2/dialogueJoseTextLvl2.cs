using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dialogueJoseTextLvl2 : MonoBehaviour
{
    public Dialogue dialogue;
    public GameObject beforeObject;
    public int point;
    public int durationCloseDoor;
    public GameObject mainPlayer;
    public GameObject BuildChurchQuest;

    void Start(){
        if(point == 0){ //control design the NPC dialogue active or sound GO active
            beforeObject.SetActive(false);
            FindObjectOfType<changueBackGroundAudio>().changueAudioBackGround(0);
            StartCoroutine(durationExitChurch1Lvl2(durationCloseDoor));
        }else{
            if(point == 4){
                beforeObject.SetActive(false);
                BuildChurchQuest.SetActive(true);
                StartCoroutine(waitAnimationMove());
            }else{
                if(point == 5){
                    MainPlayerMovement player = mainPlayer.GetComponent<MainPlayerMovement>();
                    player.ActivateMoveControll(false, false, 0, 0);
                    beforeObject.SetActive(false);
                    BuildChurchQuest.SetActive(false);
                    StartCoroutine(finishLevel());   
                }else{
                     beforeObject.SetActive(false);
                }
            }
        }
    }
    IEnumerator finishLevel(){
        yield return new WaitForSeconds(4);
        activateDialogueJoseLevel2(12);
    }
    IEnumerator waitAnimationMove(){
        yield return new WaitForSeconds(4);
        activateDialogueJoseLevel2(12);
    }

    public void TriggerJoseNPC(int numActive){
        FindObjectOfType<ControlDialogueQuests>().StartDialogueNpcFirst(dialogue, numActive);//asign correct function of classe DialogueManagerIntro
    }
    
    public void activateDialogueJoseLevel2(int numActive){
        TriggerJoseNPC(numActive);
    }
    IEnumerator durationExitChurch1Lvl2(int durationCloseDoor){
        yield return new WaitForSeconds(durationCloseDoor);
        MainPlayerMovement player = mainPlayer.GetComponent<MainPlayerMovement>();
        player.ActivateMoveControll(false, false, 0, 0);
        activateDialogueJoseLevel2(12);
    }
}
