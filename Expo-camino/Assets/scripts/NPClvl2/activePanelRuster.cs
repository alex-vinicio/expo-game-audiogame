using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class activePanelRuster : MonoBehaviour
{
    public Animator animator;

    public void activeAnimation(){
        animator.SetBool("onPanel",true);
    }

    public void disableAnimation(){
        animator.SetBool("onPanel",false);
    }
}
