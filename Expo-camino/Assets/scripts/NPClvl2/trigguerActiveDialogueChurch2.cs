using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trigguerActiveDialogueChurch2 : MonoBehaviour
{
    public GameObject beforePointObject;

    [Space]
    public AudioSource audioSource;
    public int audioDurationTemporal;
    // Start is called before the first frame update
    public GameObject activeDialogue;
    void Start()
    {
        beforePointObject.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        MainPlayer playerUnit =  hitInfo.GetComponent<MainPlayer>();
        if(hitInfo.gameObject.tag == "Player"){
            MainPlayerMovement player = playerUnit.GetComponent<MainPlayerMovement>();
            player.ActivateMoveControll(false, false, 0, 0);
            audioSource.Stop();
            audioSource.loop = false;

            dialogueJoseTextLvl2 dialogue = activeDialogue.GetComponent<dialogueJoseTextLvl2>();
            dialogue.activateDialogueJoseLevel2(12);
        }
    }
}
