using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class startQuest1OptionalLvl2 : MonoBehaviour
{
    public AudioClip[] voices;
    public int[] secondsAudios;
    public bool completeQuest;
    public bool returnPlayer = false;
    public GameObject cat;

    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;

    private int[] aux;

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        MainPlayer playerUnit =  hitInfo.GetComponent<MainPlayer>();
        if(hitInfo.gameObject.tag == "Player"){
            if(!completeQuest){
                if(returnPlayer){
                    voiceSource.loop = false;
                    specificSoundByIndex(4);
                    FindObjectOfType<updateValueLvl2>().coinPointPlayer(5);//add coins in variable auxiliar
                    StartCoroutine(destroyCat());
                    completeQuest = true;
                }else{
                    cat.SetActive(true);
                    MainPlayerMovement player = playerUnit.GetComponent<MainPlayerMovement>();
                    player.ActivateMoveControll(false, false, 0, 0);
                    voiceSource.Stop();
                    voiceSource.loop = false;
                    FindObjectOfType<dialogueTextQuestO1>().TriggerNPCQuest( playerUnit);
                }  
            }else{
                voiceSource.loop = false;
                specificSoundByIndex(5);
            }   
        }
    } 

    IEnumerator destroyCat(){
        yield return new WaitForSeconds(4);
        Destroy(cat);
    }

    public void returnPlayerQuest(){
        returnPlayer = true;
    }
    public int[] addIndexVoice(){
        return secondsAudios;
    }  
    public void ReproduceSound(int index){
        if(index >= 3){
            voiceSource.Stop();
            StopAllCoroutines();
            FindObjectOfType<MainPlayerMovement>().ActivateMoveControll(true, false, 0, 0);
            FindObjectOfType<changueAudioCat>().firstSoundCat(0);
            return;
        }

        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }
    public void specificSoundByIndex(int index){
        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
        if(index == 3){
            voiceSource.loop = true;
        }
    }
}
