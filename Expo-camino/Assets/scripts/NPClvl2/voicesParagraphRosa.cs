using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class voicesParagraphRosa : MonoBehaviour
{
    private DialogueManagerIntro dialogueManager;

    public AudioClip[] voices;
    public int[] secondsAudios;
    public AudioClip[] lifeAudio;

    public GameObject mainPlayer;
    public GameObject rosaLvl2;
    //acces to points o references
    public int timePauseDialogue;

    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;

    void Start()
    {
        dialogueManager = GetComponent<DialogueManagerIntro>();
        if(playerData.numerPosibility > 0){
            voices[3] = lifeAudio[playerData.numerPosibility];
        }
    }

    //It use for add vector seconts in other class
    public int[] addIndexVoice(){
        return secondsAudios;
    }
    public void ReproduceSound(int index){
        if(index >= secondsAudios.Length){
            voiceSource.Stop();
            StartCoroutine(waitForPauseVoice());
            return;
        }
        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }

    IEnumerator waitForPauseVoice(){
        
        yield return new WaitForSeconds(timePauseDialogue);

        MainPlayerMovement player = mainPlayer.GetComponent<MainPlayerMovement>();
        player.ActivateMoveControll(true, false, 0, 0);
        rosaLvl2.SetActive(true);
    }
}
