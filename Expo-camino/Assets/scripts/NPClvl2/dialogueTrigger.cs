using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dialogueTrigger : MonoBehaviour
{
    public int pointRosa;
    public AudioSource audioSourcePoint;

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        MainPlayer playerUnit =  hitInfo.GetComponent<MainPlayer>();
        if(hitInfo.gameObject.tag == "Player"){
            audioSourcePoint.loop = false;
            MainPlayerMovement player = playerUnit.GetComponent<MainPlayerMovement>();
            player.ActivateMoveControll(false, false, 0, 0);
            if(pointRosa == 1){
                FindObjectOfType<dialoguePoint1Lvl2>().activateDialogueRosaP1(4);
            }else{
                if(pointRosa == 2){
                    FindObjectOfType<dialoguePoint1Lvl2>().activateDialogueRosaP1(5);
                }else{
                    if(pointRosa == 3){
                        FindObjectOfType<dialogueJoseTextLvl2>().activateDialogueJoseLevel2(12);
                    }
                }
            }
        }
    }
}
