using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Globalization;

public class dialogueParagraphRosa : MonoBehaviour
{
    public Dialogue dialogue;
    public GameObject playerGame;
    public int levelUpdate;

    void Start(){
        if(playerGame){//load all resources inf the scene and wait 3s for generate the dialogue
            StartCoroutine(verifyConectServer());
        }
    }
    IEnumerator verifyConectServer(){
        MainPlayerMovement player = playerGame.GetComponent<MainPlayerMovement>();
        player.ActivateMoveControll(false, false, 0, 0);
        UnityWebRequest www = UnityWebRequest.Get("http://expocamino.azurewebsites.net/validateInternetConection/serverOn.php");//URL of service registered
        yield return www.SendWebRequest();//add new tecnologi replace a WWW obsolet
        switch (www.result)
        {
            case UnityWebRequest.Result.ConnectionError:
                activeDataElseInternet();
                break;
            case UnityWebRequest.Result.DataProcessingError:
                activeDataElseInternet();
                break;
            case UnityWebRequest.Result.ProtocolError:
                activeDataElseInternet();
                break;
            case UnityWebRequest.Result.Success:
                StartCoroutine(chargeDataPlayer());
                break;
        }
    }
    public void activeDataElseInternet(){
        FindObjectOfType<MainPlayer>().chargueAllData();
        activateDialogueStar(2);
    }
    public void TriggerRosaNPC(int numActive){
        FindObjectOfType<ControlDialogueQuests>().StartDialogueNpcFirst(dialogue, numActive);//asign correct function of classe DialogueManagerIntro
    }
    
    public void activateDialogueRosa(int numActive){
        TriggerRosaNPC(numActive);
    }

    public void activateDialogueStar(int numActive){
        TriggerRosaNPC(numActive);
    }
    //chargue alla data save in before level whit acces at server
    IEnumerator chargeDataPlayer(){
        WWWForm formU= new WWWForm();//create a form for sent Post of server
        formU.AddField("name", playerData.namePlayer);
        formU.AddField("idLevel", levelUpdate);
        UnityWebRequest www = UnityWebRequest.Post("http://expocamino.azurewebsites.net/levelquest/getDataLevel.php", formU);//URL of service registered
        yield return www.SendWebRequest();//add new tecnologi replace a WWW obsolet
        if(www.downloadHandler.text[0] == '0'){//to assign variable player to charge in player
            playerData.namePlayer = www.downloadHandler.text.Split('\t')[1];
            playerData.positionX = float.Parse(www.downloadHandler.text.Split('\t')[3],CultureInfo.InvariantCulture.NumberFormat); // convert string to float in sintax Response    
            playerData.positionY = float.Parse(www.downloadHandler.text.Split('\t')[4],CultureInfo.InvariantCulture.NumberFormat);
            playerData.numRepeticion = int.Parse(www.downloadHandler.text.Split('\t')[6]);
            playerData.distanceRoad = int.Parse(www.downloadHandler.text.Split('\t')[9]);
            playerData.nivelActual = int.Parse(www.downloadHandler.text.Split('\t')[10]);

            FindObjectOfType<MainPlayer>().chargueAllData();
            activateDialogueStar(2);
        }
    }
}
