using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro; 
using UnityEngine.Windows.Speech;
using UnityEngine.Networking;
using System.Globalization;// convert string a float
//using System.Linq;  //library for use a dictionary
//using System; //function por word Action

public class inputNamePlayer : MonoBehaviour
{
    public TextMeshProUGUI titleName;
    public GameObject registerName;
    public TextMeshProUGUI valueName;
    public int[] timeVoicesLogin;
    public bool correctName=false;
    //private bool verifyRegister = false;
    private string responseLogin;
    private string nameRequest;
    //audio
    public AudioClip[] voices;
    [Space] //use that gameObject for add audioclips and reproduction more audios
    public AudioSource voiceSource;
    //varaibles for voice dictation
    protected DictationRecognizer dictationRecognizer;
    void Start()
    {
        registerName.SetActive(true);
        StartCoroutine(verifyConectServer());
        
    }
    IEnumerator verifyConectServer(){
        UnityWebRequest www = UnityWebRequest.Get("http://expocaminobe.softjumpai.online/validateInternetConection/serverOn.php");//URL of service registered
        yield return www.SendWebRequest();//add new tecnologi replace a WWW obsolet
        switch (www.result)
        {
            case UnityWebRequest.Result.ConnectionError:
                //print(www.result);
                StartCoroutine(timiWaitLogin(5));
                break;
            case UnityWebRequest.Result.DataProcessingError:
                StartCoroutine(timiWaitLogin(5));
                break;
            case UnityWebRequest.Result.ProtocolError:
                StartCoroutine(timiWaitLogin(5));
                break;
            case UnityWebRequest.Result.Success:
                StartCoroutine(timiWaitLogin(0));
                break;
        }
    }

    public void soundPlay(int index){//function for play the audio
        voiceSource.Stop();
        voiceSource.clip = voices[index];
        voiceSource.Play();
    }
    IEnumerator timiWaitLogin(int index){
        if(index == 0){
            titleName.text = "¿Dime cuál es tu nombre, ejem: Alex?, si no te doy respuesta después de un tiempo o despues de 5 seg, ¡vuelve a responder tu nombre!";
            valueName.text = "--------";
            soundPlay(index);
        }else{
            if(index == 1){
                titleName.text = "Bienvenido al mundo de Expo-camino, aquí aprenderemos la historia de Quito usando matemática, así que empecemos con el tutorial para que conozcas los comandos a utilizar.";
                soundPlay(index);
            }else{
                if(index == 2){
                    titleName.text = "Vaya nuevamente nos encontramos. Dime ¿quieres comenzar una nueva partida responde diciendo Si o quieres continuar donde te quedaste responde No?, recuerda si después de un tiempo de tu respuesta, yo no te hablo, repite nuevamente tu respuesta.";
                    valueName.text = "--------";
                    soundPlay(index);
                }else{
                    if(index == 3){
                        titleName.text = "Continua en el mundo de Expo-camino y ayúdame a terminar la historia. empezaras donde te quedaste, suerte. Cargando el nivel guardado.";
                        soundPlay(index);
                    }else{
                        if(index == 4){
                            titleName.text = "¿Nuevamente dime tu nombre, ejem: Alex1?";
                            valueName.text = "--------";
                            soundPlay(index);
                        }else{
                            if(index == 5){
                                titleName.text = "Ocurrió un problema al guardar tus datos, una causa puede ser tu internet o es el propio expo-camino, cargando expo-camino en modo simple";
                                soundPlay(index);
                            }else{
                                if(index == 6){
                                    titleName.text = "¡Vaya veo que has completado el juego, eres el mejor, quieres mejora tu clasificación anterior y convierte en el mejor de Expo-camino, inténtalo nuevamente!";
                                    valueName.text = "Juego completado";
                                    soundPlay(index);
                                }
                            }
                        }
                    }
                }
            }
        }
        yield return new WaitForSeconds(timeVoicesLogin[index]);
        voiceSource.Stop();
        if(index == 0){
            StartDictationEngine();
        }else{
            if(index == 1){
                sceneInputNamePlayer(0);
            }else{
                if(index == 2){
                    StartDictationEngine();
                }else{
                   if(index == 3){
                       StartCoroutine(chargeDataPlayer());
                       //chargue data of player
                   }else{
                       if(index == 4){
                            StartDictationEngine();
                        }else{
                            if(index == 5){
                                playerData.namePlayer = "Anonimo";
                                playerData.numerPosibility = 7;
                                playerData.score = 0;
                                playerData.respuestasFallidas = 0;
                                playerData.nivelActual = 1;
                                playerData.conexionInternet = false;
                                playerData.distanceRoad = 0;
                                correctName = true;
                                StartCoroutine(timiWaitLogin(1));
                            }else{
                                StartCoroutine(timiWaitLogin(0));
                            }
                        }   
                   }
                }
            }
        }
    }
    
    IEnumerator chargeDataPlayer(){
        WWWForm formU= new WWWForm();//create a form for sent Post of server
        formU.AddField("name", nameRequest);
        UnityWebRequest www = UnityWebRequest.Post("http://expocaminobe.softjumpai.online/player/gestUserData.php", formU);//URL of service registered
        yield return www.SendWebRequest();//add new tecnologi replace a WWW obsolet
        if(www.downloadHandler.text[0] == '0'){//to assign variable player to charge in player
            playerData.namePlayer = www.downloadHandler.text.Split('\t')[1];
            playerData.idNivel = int.Parse(www.downloadHandler.text.Split('\t')[2]);
            playerData.positionX = float.Parse(www.downloadHandler.text.Split('\t')[3],CultureInfo.InvariantCulture.NumberFormat); // convert string to float in sintax Response    
            playerData.positionY = float.Parse(www.downloadHandler.text.Split('\t')[4],CultureInfo.InvariantCulture.NumberFormat);
            playerData.tiempoNivel = float.Parse(www.downloadHandler.text.Split('\t')[5],CultureInfo.InvariantCulture.NumberFormat);
            playerData.numRepeticion = int.Parse(www.downloadHandler.text.Split('\t')[6]);
            playerData.score = int.Parse(www.downloadHandler.text.Split('\t')[7],CultureInfo.InvariantCulture.NumberFormat);
            playerData.respuestasFallidas = int.Parse(www.downloadHandler.text.Split('\t')[8]);
            playerData.distanceRoad = int.Parse(www.downloadHandler.text.Split('\t')[9]);
            playerData.nivelActual = int.Parse(www.downloadHandler.text.Split('\t')[10]);

            sceneInputNamePlayer(playerData.nivelActual+1);
        }else
        { 
            if(www.downloadHandler.text[0] == '7'){
                StartCoroutine(timiWaitLogin(6));
            }else{
                StartCoroutine(timiWaitLogin(4));
            }
        }
    }
    private void DictationRecognizer_OnDictationHypothesis(string text)
    {
        //Debug.Log("Dictation hypothesis: " + text);
    }
    private void DictationRecognizer_OnDictationComplete(DictationCompletionCause completionCause)
    {
        switch (completionCause)
        {
            case DictationCompletionCause.TimeoutExceeded:
            case DictationCompletionCause.PauseLimitExceeded:
            case DictationCompletionCause.Canceled:
            case DictationCompletionCause.Complete:
                // Restart required
                CloseDictationEngine();
                StartDictationEngine();
                break;
            case DictationCompletionCause.UnknownError:
            case DictationCompletionCause.AudioQualityFailure:
            case DictationCompletionCause.MicrophoneUnavailable:
            case DictationCompletionCause.NetworkFailure:
                // Error
                CloseDictationEngine();
                break;
        }
    }
    private void DictationRecognizer_OnDictationResult(string text, ConfidenceLevel confidence)
    {
        valueName.text = text;
        CloseDictationEngine();
        if(valueName.text == "sí"){
            correctName = true;
            StartCoroutine(timiWaitLogin(4));
        }else{
            if(valueName.text == "no"){
                valueName.text = nameRequest;
                correctName = true;
                StartCoroutine(timiWaitLogin(3));
            }else{
                correctName = true;
                StartCoroutine(newUser());
            }
        }
    }
    IEnumerator newUser(){
        WWWForm formU= new WWWForm();//create a form for sent Post of server
        formU.AddField("name", valueName.text);
        formU.AddField("nivelVision","bajo");
        UnityWebRequest www = UnityWebRequest.Post("http://expocaminobe.softjumpai.online/player/registerU.php", formU);//URL of service registered
        yield return www.SendWebRequest();

        if(www.downloadHandler.text != ""){
            if(www.downloadHandler.text[0] == '0'){//if the name of player is new
                playerData.namePlayer = valueName.text;
                playerData.numerPosibility = 7;
                playerData.numRepeticion = 7;
                playerData.score = 0;
                playerData.positionX = 0;
                playerData.positionY = 0;
                playerData.respuestasFallidas = 0;
                playerData.nivelActual = 1;
                playerData.conexionInternet = true;
                playerData.distanceRoad = 0;
                StartCoroutine(timiWaitLogin(1));
            }else{//if name of player is registered in DB
                if(www.downloadHandler.text[0] == '3'){
                    nameRequest = valueName.text;
                    StartCoroutine(timiWaitLogin(2));
                }else{
                    StartCoroutine(timiWaitLogin(5));
                }
            }  
        }else{
            StartCoroutine(timiWaitLogin(5));//if the player don't have internet or the server is off
        }
    }
    private void DictationRecognizer_OnDictationError(string error, int hresult)
    {
        Debug.Log("Dictation error: " + error);
    }
    private void OnApplicationQuit()
    {
        CloseDictationEngine();
    }
    private void StartDictationEngine()
    {
        dictationRecognizer = new DictationRecognizer();
        dictationRecognizer.DictationHypothesis += DictationRecognizer_OnDictationHypothesis;
        dictationRecognizer.DictationResult += DictationRecognizer_OnDictationResult;
        dictationRecognizer.DictationComplete += DictationRecognizer_OnDictationComplete;
        dictationRecognizer.DictationError += DictationRecognizer_OnDictationError;
        dictationRecognizer.Start();
    }
    private void CloseDictationEngine()
    {
        if (dictationRecognizer != null)
        {
            dictationRecognizer.DictationHypothesis -= DictationRecognizer_OnDictationHypothesis;
            dictationRecognizer.DictationComplete -= DictationRecognizer_OnDictationComplete;
            dictationRecognizer.DictationResult -= DictationRecognizer_OnDictationResult;
            dictationRecognizer.DictationError -= DictationRecognizer_OnDictationError;
            if (dictationRecognizer.Status == SpeechSystemStatus.Running)
            {
                dictationRecognizer.Stop();
            }
            dictationRecognizer.Dispose();
        }
    }
    public void sceneInputNamePlayer(int numScene){
        if(correctName){
            StartCoroutine(waitForInputName(numScene));
        }
    }
    IEnumerator waitForInputName(int numScene){
        yield return new WaitForSeconds(2);
        registerName.SetActive(false);
        
        if(numScene >0){
             FindObjectOfType<levelLoad>().LoadLevel(numScene);
        }else
        {
             FindObjectOfType<levelLoad>().LoadLevel(1); //sent a scene for charge
        }
        yield return null;
    }
}
