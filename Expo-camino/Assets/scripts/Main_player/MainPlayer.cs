using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;//library for use a UI elements 
using TMPro; //library for use TMP whit element for drag in a any Script

public class MainPlayer : MonoBehaviour
{
    public int currentHealth;
    public string namePlayer;
    public int levelNow;
    public int errors;
    public float xPosition;
    public float yPosition;
    public int score;
    public int coin;

    // public TMP_Text percentaje;
    // public GameObject healthBar;
    // Start is called before the first frame update
    void Awake()
    {
        //playerData.numerPosibility = 2;
        
    }

    public void chargueAllData(){
        currentHealth = playerData.numRepeticion;
        if(FindObjectOfType<updateValueLvl2>()){
            FindObjectOfType<updateValueLvl2>().lifePointPlayer(currentHealth);
        }
        FindObjectOfType<updateValueLvl2>().scoreViewtPlayer(playerData.score);
        score = playerData.score;
        coin = playerData.coins;
        namePlayer = playerData.namePlayer;
        errors = playerData.respuestasFallidas;
        levelNow = playerData.nivelActual;
        xPosition = playerData.positionX;
        yPosition = playerData.positionY;
    }
    // Update is called once per frame
    void Update()
    {
        levelNow = playerData.nivelActual;
    }

    public int TakeDamage(int damage){// restar liffe or number or posibility at player game.
        currentHealth = currentHealth - damage;
        return currentHealth;
    }
    public bool lifeValidate(){
        //FindObjectOfType<updateValueLvl2>().lifePointPlayer(currentHealth);
        if(currentHealth > 0){
            return false;
        }else{
            return true;
        }
    }

    public void errorUpdate(int errorGet){//count all error in all levels, if player lose lifes and restar level the count error continue
        playerData.respuestasFallidas = playerData.respuestasFallidas + errorGet;
        errors = playerData.respuestasFallidas;
    }

    public int coinAdd(int coinValue){// add coin in player object
        coin = coin + coinValue;
        return coin;
    }

    public int scoreAdd(int scoreValue){//add score in values of player object
       score= score + scoreValue;
        return score;
    }

    /*private void OnTriggerEnter2D(Collider2D money)
    {
        if (money.gameObject.CompareTag("Money"))
        {
            Destroy(money.gameObject);
        }
    }*/
}
