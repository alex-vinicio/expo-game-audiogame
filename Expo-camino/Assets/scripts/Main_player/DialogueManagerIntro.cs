using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;

public class DialogueManagerIntro : MonoBehaviour
{
    public TextMeshProUGUI dialogText;
    public GameObject canvasDialog;
    private Queue<string> sentences;

    //control for automatic dialog
    private bool activeDialogue = false;
    private int indexVoices,index;
    private int[] valueTimeVoice;
    private int tipeNPCAudio;
    private int tipeShortDialog = 0;
    //control for a button continuo dialog
    public GameObject buttonActionsDIalog;
    string sentence;

    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        buttonActionsDIalog.SetActive(false);
        sentences = new Queue<string>();
    }

    void Update(){        
        if(activeDialogue){
            if(index < valueTimeVoice.Length){
                activeDialogue=false;// active for stop the update transition in coroutine
                StartCoroutine(activation(index));
            }
        }
    
        if(dialogText.text == sentence){
            buttonActionsDIalog.SetActive(true);
        }
    }    

    IEnumerator activation(int index){
        yield return new WaitForSeconds(valueTimeVoice[index]);
        activeDialogue=true;//active dialogue and change next dialogue
        DisplayNextStentence();
        buttonActionsDIalog.SetActive(false);
    }
    //player dialogue 1
    public void StartDialogueRobot(Dialogue dialogue, MainPlayer playerUnit){
        index = 0;
        indexVoices =0;
        tipeNPCAudio = 1;
        valueTimeVoice = changueSoundDurationGeneral(tipeNPCAudio);
        canvasDialog.SetActive(true);//very important the first go gameObject or prefab active and lest continue animation
        animator.SetBool("isOpen",true);//active animation of camvas

        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        
        
        DisplayNextStentence();  
        activeDialogue = true;
    }
    //Ivibot dialogue 1
    public void StartDialogueCow(Dialogue dialogue, MainPlayer playerUnit){
        index = 0;
        indexVoices =0;
        tipeNPCAudio = 2;
        valueTimeVoice = changueSoundDurationGeneral(tipeNPCAudio);
        canvasDialog.SetActive(true);//very important the first go gameObject or prefab active and lest continue animation
        animator.SetBool("isOpen",true);//active animation of camvas

        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        activeDialogue = true;
        
        DisplayNextStentence();  
    }
    //Ivibot dialogue 2
    public void StartDialogueIvibot2(Dialogue dialogue, MainPlayer playerUnit, int valDialogueShort){
        index = 0;
        indexVoices =0;
        tipeNPCAudio = 3;
        valueTimeVoice = null;
        tipeShortDialog = valDialogueShort;
        valueTimeVoice = changueSoundDurationGeneral(tipeNPCAudio);
        canvasDialog.SetActive(true);//very important the first go gameObject or prefab active and lest continue animation
        animator.SetBool("isOpen",true);//active animation of camvas

        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        activeDialogue = true;
        
        DisplayNextStentence();  
    }
    public void ActiveNextStentence(bool activeDialog){
        activeDialogue = activeDialog;
        DisplayNextStentence();
    }
    public void DisplayNextStentence(){
        if(sentences.Count == 0){
            EndDialogue();
            index = 0;
            return;
        }

        sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }
    //control other objects
    public void changueSoundActivationGeneral(int tipeSound){
        if(tipeSound == 1){
            if(FindObjectOfType<SoundManagerRobot>())
                FindObjectOfType<SoundManagerRobot>().ReproduceSound(indexVoices);
        }else{
            if(tipeSound == 2){
                if(FindObjectOfType<SoundManagerIntroFirst>()){
                    FindObjectOfType<SoundManagerIntroFirst>().ReproduceSound(indexVoices);
                }
            }else{
                if(tipeSound == 3){
                    if(tipeShortDialog == 1){
                        if(FindObjectOfType<SoundManagerIntroFirst2>()){
                            FindObjectOfType<SoundManagerIntroFirst2>().ReproduceSound(indexVoices);
                        }
                    }else{
                        if(tipeShortDialog == 2){
                            if(FindObjectOfType<SoundManagerIntroFirst3>()){
                                FindObjectOfType<SoundManagerIntroFirst3>().ReproduceSound(indexVoices);
                            }
                        }else{
                            if(tipeShortDialog == 3){
                                if(FindObjectOfType<SoundManagerIntroFirst4>()){
                                    FindObjectOfType<SoundManagerIntroFirst4>().ReproduceSound(indexVoices);
                                }
                            }else{
                                if(tipeShortDialog == 4){
                                    if(FindObjectOfType<SoundManagerIntroFirst5>()){
                                        FindObjectOfType<SoundManagerIntroFirst5>().ReproduceSound(indexVoices);
                                    }
                                }else{
                                    if(FindObjectOfType<SoundManagerIntroFirst6>()){
                                        FindObjectOfType<SoundManagerIntroFirst6>().ReproduceSound(indexVoices);
                                    }
                                }
                            }

                        }
                    }
                    if(FindObjectOfType<SoundManagerIntroFirst2>()){
                        FindObjectOfType<SoundManagerIntroFirst2>().ReproduceSound(indexVoices);
                    }
                }
            }
        }
    }

    public int[] changueSoundDurationGeneral(int tipeSound){
        if(tipeSound == 1){
            if(FindObjectOfType<SoundManagerRobot>())
                return  FindObjectOfType<SoundManagerRobot>().valueTimeVoices();
            else
                return null;
        }else{
            if(tipeSound == 2){
                if(FindObjectOfType<SoundManagerIntroFirst>()){
                    return FindObjectOfType<SoundManagerIntroFirst>().valueTimeVoices();
                }else{
                    return null;
                }
            }else{
                if(tipeSound == 3){
                    if(tipeShortDialog == 1){
                        if(FindObjectOfType<SoundManagerIntroFirst2>()){
                            return FindObjectOfType<SoundManagerIntroFirst2>().valueTimeVoices();
                        }else{
                            return null;
                        }
                    }else{
                        if(tipeShortDialog == 2){
                            if(FindObjectOfType<SoundManagerIntroFirst3>()){
                                return FindObjectOfType<SoundManagerIntroFirst3>().valueTimeVoices();
                            }else{
                                return null;
                            }
                        }else{
                            if(tipeShortDialog == 3){
                                if(FindObjectOfType<SoundManagerIntroFirst4>()){
                                    return FindObjectOfType<SoundManagerIntroFirst4>().valueTimeVoices();
                                }else{
                                    return null;
                                }
                            }else{
                                if(tipeShortDialog == 4){
                                    if(FindObjectOfType<SoundManagerIntroFirst5>()){
                                        return FindObjectOfType<SoundManagerIntroFirst5>().valueTimeVoices();
                                    }else{
                                        return null;
                                    }
                                }else{
                                    if(FindObjectOfType<SoundManagerIntroFirst6>()){
                                        return FindObjectOfType<SoundManagerIntroFirst6>().valueTimeVoices();
                                    }else{
                                        return null;
                                    }
                                }
                            }

                        }
                    }
                }else{
                    return null;
                }
            }
        }
    }
    IEnumerator TypeSentence(string sentence){
        dialogText.text = "";
        buttonActionsDIalog.SetActive(false);
        changueSoundActivationGeneral(tipeNPCAudio);
        indexVoices++;
        foreach (char letter in sentence.ToCharArray())
        {
             dialogText.text += letter; //important add + for add in the TMP letter for letter
             yield return null;
        }
        index++; //count for control of index of durations audio vectors
        
    }

    void EndDialogue(){
        animator.SetBool("isOpen",false);
        changueSoundActivationGeneral(tipeNPCAudio);
        activeDialogue=false;
    }
}
