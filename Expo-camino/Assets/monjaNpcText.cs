using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monjaNpcText : MonoBehaviour
{
    public Dialogue dialogue;
    public int point;
    public GameObject beforeObjectChurch;

    void Start()
    {
        if (point == 0)
        {
            StartCoroutine(waitTimeCloseDoor(FindObjectOfType<triggerChurchOpenDor>().valueTimeForDurationDoorAudio()[1]));
        }
        else
        {
            beforeObjectChurch.SetActive(false);
        }
    }

    public void TriggerPastorsNPC(int numActive)
    {
        FindObjectOfType<ControlDialog>().StartDialogueNpcFirst(dialogue, numActive);//asign correct function of classe DialogueManagerIntro
    }

    public void activateDialogueMonja(int numActive)
    {
        TriggerPastorsNPC(numActive);
    }
    IEnumerator waitTimeCloseDoor(int duration)
    {
        yield return new WaitForSeconds(duration);
        activateDialogueMonja(6);
        beforeObjectChurch.SetActive(false);
        FindObjectOfType<changueBackGroundAudio>().changueAudioBackGround(1);//active changue other audio background
    }
}
